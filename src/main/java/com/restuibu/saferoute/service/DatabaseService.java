package com.restuibu.saferoute.service;

import com.restuibu.saferoute.core.DataWilayah;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface DatabaseService {
    //public long jarak(String awal, String tujuan);
    //public long nilaiHeuristik(String awal, String tujuan);
    public String[] tetangga(String wilayah);
    public List<DataWilayah> showAllKecamatan();
    public void inputIt() throws FileNotFoundException, IOException;
    public void calculateCoronaLevel();
}
