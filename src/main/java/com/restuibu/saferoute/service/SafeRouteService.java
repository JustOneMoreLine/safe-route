package com.restuibu.saferoute.service;

public interface SafeRouteService {
    public String safeRouteAtoB(String a, String b);
    public String shortRouteAtoB(String a, String b);
}
