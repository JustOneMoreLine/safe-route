package com.restuibu.saferoute.service;

import com.restuibu.saferoute.core.*;
import com.restuibu.saferoute.repository.*;
import com.restuibu.saferoute.service.data.DataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Kelas yang mengandung semua pengambilan dan penyimpanan data
 * dari database.
 */
@Component
public class DatabaseServiceImpl implements DatabaseService{
    private static DataWilayahRepository dataWilayahRepository;
    private static DataCoronaRepository dataCoronaRepository;
    private static DataTetanggaAsliRepository dataTetanggaAsliRepository;
    private static DataHeuristikRepository dataHeuristikRepository;
    private static DataLevelRepository dataLevelRepository;
    private static List<DataInput> inputList;
    private static HashMap<String, Double>memoJarak;
    private static HashMap<String, Double>memoHeu;

    @Autowired
    public DatabaseServiceImpl(DataWilayahRepository dataWilayahRepository,
                               DataCoronaRepository dataCoronaRepository,
                               DataTetanggaAsliRepository dataTetanggaAsli,
                               DataHeuristikRepository dataHeuristikRepository,
                               DataLevelRepository dataLevelRepository,
                               List<DataInput> list) {
        this.dataCoronaRepository = dataCoronaRepository;
        this.dataWilayahRepository = dataWilayahRepository;
        this.dataTetanggaAsliRepository= dataTetanggaAsli;
        this.dataHeuristikRepository = dataHeuristikRepository;
        this.dataLevelRepository = dataLevelRepository;
        this.inputList = list;
        this.memoJarak = new HashMap<>();
        this.memoHeu = new HashMap<>();
    }

    /*public long jarak(String awal, String tujuan) {
        // TODO DataWilayah wilayahAwal = dataWilayahRepository.findBy
        return 0;
    }*/


    /*public long nilaiHeuristik(String awal, String tujuan) {
        // TODO
        return 0;
    }*/

    @Override
    public String[] tetangga(String wilayah) {
        return dataTetanggaAsliRepository.getOne(wilayah).getTetangga();
    }

    @Override
    public List<DataWilayah> showAllKecamatan() {
        return dataWilayahRepository.findAll();
    }

    @Override
    public void inputIt() throws FileNotFoundException, IOException {
        for(DataInput x : inputList) {
            x.inputData();
        }
    }

    @Override
    public void calculateCoronaLevel() {
        if(isLevelDataEmpty()) {
            // TODO : Create new data
            createNewLevelData();
        } else {
            // TODO : update data
        }
    }

    private boolean isLevelDataEmpty() {
        if(dataLevelRepository.findAll().isEmpty()) return true;
        return false;
    }

    private void createNewLevelData() {
        for(DataWilayah x : dataWilayahRepository.findAll()) {
            DataLevel y = createLevelByNewestData(x);
            dataLevelRepository.saveAndFlush(y);
        }
    }

    private DataLevel createLevelByNewestData(DataWilayah dataWilayah) {
        DataCorona terbaru=null;
        //System.out.println(dataWilayah.getKecamatan());
        for(DataCorona x : dataCoronaRepository.findByIdKecamatan(dataWilayah.getKecamatan())) {
            if(terbaru == null) {
                terbaru = x;
                continue;
            }
            if(x.getTahun() >= terbaru.getTahun()) {
                if (x.getBulan() > terbaru.getBulan()) {
                    terbaru = x;
                } else {
                    continue;
                }
            } else {
                continue;
            }
        }
        //terbaru = dataCoronaRepository.findByIdKecamatan(dataWilayah.getKecamatan());
        //System.out.println("Loop in: " + terbaru.getIdKecamatan());
        /*for(DataCorona x : dataCoronaRepository.findAll()) {
            System.out.println(x.getIdKecamatan() + " " + x.getPositif());
        }*/
        return createLevel(terbaru);
    }

    private DataLevel createLevel(DataCorona dataCorona) {
        String kecamatan = dataCorona.getIdKecamatan();
        long bulan = dataCorona.getBulan();
        long tahun = dataCorona.getTahun();
        int level = 0;
        long populasiKecamatan = dataWilayahRepository.getOne(kecamatan).getPopulasi();
        if(populasiKecamatan > 200000) {
            level = incidenceFormula(dataCorona);
        } else {
            level = caseCountFormula(dataCorona);
        }
        return new DataLevel(kecamatan, level, level, bulan, tahun);
    }

    private int incidenceFormula(DataCorona dataCorona) {
        double incidence = (dataCorona.getSelfIsolation()+dataCorona.getDirawat())/100000;
        if (incidence < 5) {
            return 1;
        } else if (incidence <= 50) {
            return 2;
        } else if (incidence <= 100) {
            return 3;
        } else
            return 4;
    }

    private int caseCountFormula(DataCorona dataCorona) {
        long caseCount = dataCorona.getSelfIsolation()+dataCorona.getDirawat();
        if (caseCount < 10) {
            return 1;
        } else if (caseCount <= 50) {
            return 2;
        } else if (caseCount <= 100) {
            return 3;
        } else {
            return 4;
        }
    }

    public static Double getJarak(String awal, String tujuan) {
        //System.out.println("=============================================");
        //System.out.println("Jarak");
        //System.out.println("Kota awal: " + awal);
        //System.out.println("Kota tujuan: " + tujuan);
        String key = awal+"-"+tujuan;
        if(memoJarak.get(key) == null) {
            //System.out.println("memo Null");
            DataTetanggaAsli wilayahAwal = dataTetanggaAsliRepository.getOne(awal);
            if (wilayahAwal == null) return null;
            //System.out.println("wilayah not null");
            Double result = wilayahAwal.getJarakKeTetangga().get(tujuan);
            memoJarak.put(key, result);
            //System.out.println(result);
            //System.out.println("=============================================");
            return result;
        } else {
            //System.out.println("memo not null");
            //System.out.println(memoHeu.get(key));
            //System.out.println("=============================================");
            return memoJarak.get(key);
        }
    }
    public static Double nilaiHeuristik(String awal, String tujuan) {
        //System.out.println("=============================================");
        //System.out.println("nilaiHeuristik");
        //System.out.println("Kota awal: " + awal);
        //System.out.println("Kota tujuan: " + tujuan);
        String key = awal+"-"+tujuan;
        if(memoHeu.get(key) == null) {
            //System.out.println("memo Null");
            DataHeuristik wilayahAwal = dataHeuristikRepository.getOne(awal);
            if (wilayahAwal == null) return null;
            //System.out.println("wilayah not null");
            Double result = wilayahAwal.getJarakKeTetangga().get(tujuan);
            memoHeu.put(key, result);
            //System.out.println(result);
            //System.out.println("=============================================");
            return result;
        } else {
            //System.out.println("memo not null");
            //System.out.println(memoHeu.get(key));
            //System.out.println("=============================================");
            return memoHeu.get(key);
        }
    }

    public static int nilaiLevel(String kecamatan) {
        DataLevel x = dataLevelRepository.getOne(kecamatan);
        return x.getLevelSekarang();
    }
}
