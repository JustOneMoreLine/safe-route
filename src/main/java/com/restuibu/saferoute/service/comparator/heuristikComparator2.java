package com.restuibu.saferoute.service.comparator;

import com.restuibu.saferoute.core.Path;

import java.util.Comparator;

public class heuristikComparator2 implements Comparator<Path> {
    @Override
    public int compare(Path o1, Path o2) {
        if(o1.getNilaiHeuristik2() < o2.getNilaiHeuristik2()) {
            return -1;
        } else if(o1.getNilaiHeuristik2() > o2.getNilaiHeuristik2()) {
            return 1;
        } else { // sama nilainya
            return 0;
        }
    }
}
