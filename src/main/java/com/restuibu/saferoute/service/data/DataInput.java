package com.restuibu.saferoute.service.data;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface DataInput {
    public void inputData() throws FileNotFoundException, IOException;
}
