package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataCorona;
import com.restuibu.saferoute.repository.DataCoronaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DataInputCorona implements DataInput {
    DataCoronaRepository DataCoronaRepo;
    @Autowired
    public DataInputCorona(DataCoronaRepository DataCoronaRepo){
        this.DataCoronaRepo = DataCoronaRepo;
    }
    @Override
    public void inputData() throws FileNotFoundException,IOException{
        int count = 0; //FOR DEBUGGING PURPOSES
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        try{
            //System.out.println(new File(".").getAbsoluteFile());
            BufferedReader scan = new BufferedReader(new FileReader("src/main/java/com/restuibu/saferoute/service/data/DataCorona.txt"));
            String line;
            while((line = scan.readLine()) != null) {
                String TrueLine = line.substring(1, line.length() - 2);
                //System.out.println("---------- LINE ---------" + TrueLine);
                String[] split_data = TrueLine.split(",");
                DataCorona x = new DataCorona(
                        formatted(split_data[3]),
                        (long) localDate.getDayOfMonth(),
                        (long) localDate.getMonthValue(),
                        (long) localDate.getYear(),
                        Long.parseLong(split_data[25]),
                        Long.parseLong(split_data[26]),
                        Long.parseLong(split_data[27]),
                        Long.parseLong(split_data[28]),
                        Long.parseLong(split_data[29]));
                /*DataCorona x = new DataCorona();
                x.setIdKecamatan(formatted(split_data[3]));
                x.setTanggal(localDate.getDayOfMonth());
                x.setBulan(localDate.getMonthValue());
                x.setTahun(localDate.getYear());
                x.setPositif(Long.parseLong(split_data[25]));
                x.setDirawat(Long.parseLong(split_data[26]));
                x.setSembuh(Long.parseLong(split_data[27]));
                x.setMeninggal(Long.parseLong(split_data[28]));
                x.setSelfIsolation(Long.parseLong(split_data[29]));*/
                DataCoronaRepo.saveAndFlush(x);
                //System.out.println("DATA CORONA MASUK! " + x.getIdKecamatan());
            }
        }
        catch(FileNotFoundException error){
            //System.out.println("Can't find DataCorona.txt.");
        }
    }
    private String formatted(String name) {
        String[] splitted_name = name.split(" ");
        int counter = 0;
        String result = null;
        do {
            String str = splitted_name[counter];
            if(result != null) {
                result = result + " " + str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
            } else {
                result = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
            }
            counter++;
        } while (counter < splitted_name.length);
        return result;
    }
}
