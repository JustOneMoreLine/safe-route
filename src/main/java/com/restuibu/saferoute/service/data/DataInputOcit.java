package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataHeuristik;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.repository.DataHeuristikRepository;
import com.restuibu.saferoute.repository.DataTetanggaAsliRepository;
import com.restuibu.saferoute.repository.DataWilayahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DataInputOcit implements DataInput {
    DataWilayahRepository DataWilayahRepo;
    DataTetanggaAsliRepository DataTetanggaAsliRepo;
    DataHeuristikRepository DataHeuristikRepo;

    @Autowired
    public DataInputOcit(
            DataWilayahRepository dataWilayahRepository,
            DataTetanggaAsliRepository dataTetanggaAsliRepository,
            DataHeuristikRepository dataHeuristikRepository) {
        DataWilayahRepo = dataWilayahRepository;
        DataTetanggaAsliRepo = dataTetanggaAsliRepository;
        DataHeuristikRepo = dataHeuristikRepository;
    }

    @Override
    public void inputData() {
        DataWilayah tambora = new DataWilayah("Tambora", 241889L);
        tambora.setKecamatan("Tambora");
        DataWilayahRepo.saveAndFlush(tambora);

        DataWilayah tamanSari = new DataWilayah("Taman Sari", 102529L);
        tamanSari.setKecamatan("Taman Sari");
        DataWilayahRepo.saveAndFlush(tamanSari);

        DataWilayah cengkareng = new DataWilayah("Cengkareng", 601156L);
        cengkareng.setKecamatan("Cengkareng");
        DataWilayahRepo.saveAndFlush(cengkareng);

        DataWilayah kalideres = new DataWilayah("Kalideres", 471436L);
        kalideres.setKecamatan("Kalideres");
        DataWilayahRepo.saveAndFlush(kalideres);

        DataWilayah penjaringan = new DataWilayah("Penjaringan", 351117L);
        penjaringan.setKecamatan("Penjaringan");
        DataWilayahRepo.saveAndFlush(penjaringan);

        DataWilayah pademangan = new DataWilayah("Pademangan", 166762L);
        pademangan.setKecamatan("Pademangan");
        DataWilayahRepo.saveAndFlush(pademangan);

        DataWilayah tanjungPriok = new DataWilayah("Tanjung Priok", 394043L);
        tanjungPriok.setKecamatan("Tanjung Priok");
        DataWilayahRepo.saveAndFlush(tanjungPriok);

        DataWilayah koja = new DataWilayah("Koja", 316704L);
        koja.setKecamatan("Koja");
        DataWilayahRepo.saveAndFlush(koja);

        DataWilayah kelapaGading = new DataWilayah("Kelapa Gading", 158931L);
        kelapaGading.setKecamatan("Kelapa Gading");
        DataWilayahRepo.saveAndFlush(kelapaGading);

        DataWilayah cilincing = new DataWilayah("Cilincing", 425358L);
        cilincing.setKecamatan("Cilincing");
        DataWilayahRepo.saveAndFlush(cilincing);

        Map<String, Double> tambora_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 20.81);
            put("Pasarminggu", 16.15);
            put("Cilandak", 15.06);
            put("Pesanggrahan", 11.85);
            put("Kebayoran Lama", 10.99);
            put("Kebayoran Baru", 10.52);
            put("Mampang Prapatan", 11.46);
            put("Pancoran", 12.48);
            put("Tebet", 10.42);
            put("Setiabudi", 8.49);
            put("Pasar Rebo", 20.55);
// --------------------------------
            put("Ciracas", 20.76);
            put("Cipayung", 22.55);
            put("Makasar", 16.92);
            put("Kramat Jati", 16.07);
            put("Jatinegara", 12.62);
            put("Duren Sawit", 15.31);
            put("Cakung", 16.32);
            put("Pulogadung", 10.91);
            put("Matraman", 8.79);
            put("Tanah Abang", 5.95);
            put("Menteng", 6.13);
// --------------------------------
            put("Senen", 7.10);
            put("Johar Baru", 7.27);
            put("Cempaka Putih", 8.00);
            put("Kemayoran", 4.97);
            put("Sawah Besar", 3.19);
            put("Gambir", 2.63);
            put("Kembangan", 8.34);
            put("Kebon Jeruk", 5.75);
            put("Palmerah", 4.59);
            put("Grogol Petamburan", 2.53);
// --------------------------------
            put("Tambora", 0.0);
            put("Taman Sari", 1.87);
            put("Cengkareng", 7.46);
            put("Kalideres", 10.92);
            put("Penjaringan", 2.25);
            put("Pademangan", 4.34);
            put("Tanjung Priok", 7.77);
            put("Koja", 11.95);
            put("Kelapa Gading", 11.44);
            put("Cilincing", 16.39);
        }};

        DataHeuristik tambora_heu = new DataHeuristik("Tambora", tambora_hash);
        tambora_heu.setKecamatan("Tambora");
        DataHeuristikRepo.saveAndFlush(tambora_heu);

        Map<String, Double> tamansari_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 20.80);
            put("Pasarminggu", 15.67);
            put("Cilandak", 15.29);
            put("Pesanggrahan", 12.87);
            put("Kebayoran Lama", 11.75);
            put("Kebayoran Baru", 10.75);
            put("Mampang Prapatan", 11.31);
            put("Pancoran", 11.76);
            put("Tebet", 9.63);
            put("Setiabudi", 7.99);
            put("Pasar Rebo", 20.18);
// --------------------------------
            put("Ciracas", 20.20);
            put("Cipayung", 21.86);
            put("Makasar", 15.92);
            put("Kramat Jati", 15.56);
            put("Jatinegara", 11.44);
            put("Duren Sawit", 13.98);
            put("Cakung", 14.51);
            put("Pulogadung", 9.27);
            put("Matraman", 7.62);
            put("Tanah Abang", 6.11);
            put("Menteng", 5.50);
// --------------------------------
            put("Senen", 5.99);
            put("Johar Baru", 5.91 );
            put("Cempaka Putih", 6.41);
            put("Kemayoran", 3.21);
            put("Sawah Besar", 1.41);
            put("Gambir", 2.51);
            put("Kembangan", 10.01);
            put("Kebon Jeruk", 7.26);
            put("Palmerah", 5.31);
            put("Grogol Petamburan", 4.26);
// --------------------------------
            put("Tambora", 1.95);
            put("Taman Sari", 0.0);
            put("Cengkareng", 9.44);
            put("Kalideres", 12.81);
            put("Penjaringan", 2.96);
            put("Pademangan", 2.63);
            put("Tanjung Priok", 5.89);
            put("Koja", 10.06);
            put("Kelapa Gading", 9.47);
            put("Cilincing", 14.35);
        }};

        DataHeuristik tamansari_heu = new DataHeuristik("Taman Sari", tamansari_hash);
        tamansari_heu.setKecamatan("Taman Sari");
        DataHeuristikRepo.saveAndFlush(tamansari_heu);

        Map<String, Double> cengkareng_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 22.88);
            put("Pasarminggu", 19.67);
            put("Cilandak", 16.69);
            put("Pesanggrahan", 11.36);
            put("Kebayoran Lama", 11.58);
            put("Kebayoran Baru", 12.78);
            put("Mampang Prapatan", 14.76);
            put("Pancoran", 16.91);
            put("Tebet", 15.47);
            put("Setiabudi", 13.31);
            put("Pasar Rebo", 23.85);
// --------------------------------
            put("Ciracas", 24.54);
            put("Cipayung", 26.96);
            put("Makasar", 22.26);
            put("Kramat Jati", 20.21);
            put("Jatinegara", 18.68);
            put("Duren Sawit", 21.94);
            put("Cakung", 23.77);
            put("Pulogadung", 17.96);
            put("Matraman", 15.26);
            put("Tanah Abang", 10.37);
            put("Menteng", 12.03);
// --------------------------------
            put("Senen", 13.67);
            put("Johar Baru", 14.17 );
            put("Cempaka Putih", 15.15);
            put("Kemayoran", 12.45);
            put("Sawah Besar", 10.72);
            put("Gambir", 9.19);
            put("Kembangan", 5.00);
            put("Kebon Jeruk", 5.07);
            put("Palmerah", 8.29);
            put("Grogol Petamburan", 5.64);
// --------------------------------
            put("Tambora", 7.49);
            put("Taman Sari", 9.48);
            put("Cengkareng", 0.0);
            put("Kalideres", 3.63);
            put("Penjaringan", 7.82);
            put("Pademangan", 11.57);
            put("Tanjung Priok", 15.16);
            put("Koja", 19.23);
            put("Kelapa Gading", 18.89);
            put("Cilincing", 23.68);
        }};

        DataHeuristik cengkareng_heu = new DataHeuristik("Cengkareng", cengkareng_hash);
        cengkareng_heu.setKecamatan("Cengkareng");
        DataHeuristikRepo.saveAndFlush(cengkareng_heu);


        Map<String, Double> kalideres_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 25.30);
            put("Pasarminggu", 21.73);
            put("Cilandak", 20.01);
            put("Pesanggrahan", 14.84);
            put("Kebayoran Lama", 15.06);
            put("Kebayoran Baru", 16.05);
            put("Mampang Prapatan", 18.47);
            put("Pancoran", 20.63);
            put("Tebet", 19.98);
            put("Setiabudi", 16.71);
            put("Pasar Rebo", 26.83);
// --------------------------------
            put("Ciracas", 28.89);
            put("Cipayung", 30.23);
            put("Makasar", 24.79);
            put("Kramat Jati", 24.21);
            put("Jatinegara", 21.99);
            put("Duren Sawit", 26.40);
            put("Cakung", 26.94);
            put("Pulogadung", 21.76);
            put("Matraman", 19.50);
            put("Tanah Abang", 14.15);
            put("Menteng", 15.83);
// --------------------------------
            put("Senen", 16.56);
            put("Johar Baru", 17.77);
            put("Cempaka Putih", 19.12);
            put("Kemayoran", 17.42);
            put("Sawah Besar", 14.75);
            put("Gambir", 13.30);
            put("Kembangan", 7.43);
            put("Kebon Jeruk", 9.75);
            put("Palmerah", 11.89);
            put("Grogol Petamburan", 10.06);
// --------------------------------
            put("Tambora", 11.05);
            put("Taman Sari", 12.97);
            put("Cengkareng", 5.09);
            put("Kalideres", 0.00);
            put("Penjaringan", 7.61);
            put("Pademangan", 14.12);
            put("Tanjung Priok", 18.74);
            put("Koja", 22.84);
            put("Kelapa Gading", 22.38);
            put("Cilincing", 26.89);
        }};

        DataHeuristik kalideres_heu = new DataHeuristik("Kalideres", kalideres_hash);
        kalideres_heu.setKecamatan("Kalideres");
        DataHeuristikRepo.saveAndFlush(kalideres_heu);


        Map<String, Double> penjaringan_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 22.56);
            put("Pasarminggu", 15.92);
            put("Cilandak", 17.97);
            put("Pesanggrahan", 14.79);
            put("Kebayoran Lama", 13.71);
            put("Kebayoran Baru", 12.78);
            put("Mampang Prapatan", 13.82);
            put("Pancoran", 15.11);
            put("Tebet", 12.51);
            put("Setiabudi",10.72);
            put("Pasar Rebo", 22.60);
// --------------------------------
            put("Ciracas", 23.86);
            put("Cipayung", 24.62);
            put("Makasar", 18.31);
            put("Kramat Jati", 18.02);
            put("Jatinegara", 13.95);
            put("Duren Sawit", 17.48);
            put("Cakung", 16.51);
            put("Pulogadung", 12.00);
            put("Matraman", 10.84);
            put("Tanah Abang", 8.59);
            put("Menteng", 8.12);
// --------------------------------
            put("Senen", 7.75);
            put("Johar Baru", 8.45);
            put("Cempaka Putih", 9.39);
            put("Kemayoran", 7.13);
            put("Sawah Besar", 4.61);
            put("Gambir", 5.02);
            put("Kembangan", 9.79);
            put("Kebon Jeruk", 8.00);
            put("Palmerah", 7.02);
            put("Grogol Petamburan", 4.27);
// --------------------------------
            put("Tambora", 2.03);
            put("Taman Sari", 2.66);
            put("Cengkareng", 6.94);
            put("Kalideres", 11.19);
            put("Penjaringan", 0.0);
            put("Pademangan", 2.94);
            put("Tanjung Priok", 7.57);
            put("Koja", 11.65);
            put("Kelapa Gading", 11.62);
            put("Cilincing", 15.72);
        }};

        DataHeuristik penjaringan_heu = new DataHeuristik("Penjaringan", penjaringan_hash);
        penjaringan_heu.setKecamatan("Penjaringan");
        DataHeuristikRepo.saveAndFlush(penjaringan_heu);


        Map<String, Double> pademangan_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 22.05);
            put("Pasarminggu", 14.55);
            put("Cilandak", 18.08);
            put("Pesanggrahan", 16.01);
            put("Kebayoran Lama", 14.60);
            put("Kebayoran Baru", 13.02);
            put("Mampang Prapatan", 13.27);
            put("Pancoran", 13.97);
            put("Tebet", 10.62);
            put("Setiabudi",10.00);
            put("Pasar Rebo", 21.47);
// --------------------------------
            put("Ciracas", 22.33);
            put("Cipayung", 22.81);
            put("Makasar", 16.42);
            put("Kramat Jati", 16.28);
            put("Jatinegara", 11.57);
            put("Duren Sawit", 14.40);
            put("Cakung", 12.69);
            put("Pulogadung", 10.60);
            put("Matraman", 8.41);
            put("Tanah Abang", 8.69);
            put("Menteng", 6.97);
// --------------------------------
            put("Senen", 5.84);
            put("Johar Baru", 5.92);
            put("Cempaka Putih", 6.34);
            put("Kemayoran", 3.90);
            put("Sawah Besar", 2.64);
            put("Gambir", 4.91);
            put("Kembangan", 12.65);
            put("Kebon Jeruk", 10.11);
            put("Palmerah", 8.12);
            put("Grogol Petamburan", 6.56);
// --------------------------------
            put("Tambora", 4.42);
            put("Taman Sari", 2.67);
            put("Cengkareng", 10.61);
            put("Kalideres", 15.15);
            put("Penjaringan", 8.04);
            put("Pademangan", 0.0);
            put("Tanjung Priok", 3.60);
            put("Koja", 7.74);
            put("Kelapa Gading", 12.13);
            put("Cilincing", 11.94);
        }};

        DataHeuristik pademangan_heu = new DataHeuristik("Pademangan", pademangan_hash);
        pademangan_heu.setKecamatan("Pademangan");
        DataHeuristikRepo.saveAndFlush(pademangan_heu);


        Map<String, Double> tanjungpriok_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 22.68);
            put("Pasarminggu", 14.68);
            put("Cilandak", 19.37);
            put("Pesanggrahan", 18.19);
            put("Kebayoran Lama", 16.61);
            put("Kebayoran Baru", 14.65);
            put("Mampang Prapatan", 14.24);
            put("Pancoran", 14.36);
            put("Tebet", 10.54);
            put("Setiabudi",11.13);
            put("Pasar Rebo", 21.55);
// --------------------------------
            put("Ciracas", 21.99);
            put("Cipayung", 22.17);
            put("Makasar", 15.91);
            put("Kramat Jati", 15.95);
            put("Jatinegara", 11.57);
            put("Duren Sawit", 12.54);
            put("Cakung", 9.68);
            put("Pulogadung", 8.26);
            put("Matraman", 7.98);
            put("Tanah Abang", 10.61);
            put("Menteng", 8.21);
// --------------------------------
            put("Senen", 6.63);
            put("Johar Baru", 5.93);
            put("Cempaka Putih", 5.46);
            put("Kemayoran", 3.72);
            put("Sawah Besar", 4.91);
            put("Gambir", 7.41);
            put("Kembangan", 15.87);
            put("Kebon Jeruk", 13.07);
            put("Palmerah", 10.72);
            put("Grogol Petamburan", 9.82);
// --------------------------------
            put("Tambora", 7.91);
            put("Taman Sari", 6.02);
            put("Cengkareng", 14.17);
            put("Kalideres", 18.76);
            put("Penjaringan", 11.58);
            put("Pademangan", 4.67);
            put("Tanjung Priok", 0.0);
            put("Koja", 4.21);
            put("Kelapa Gading", 4.65);
            put("Cilincing", 8.13);
        }};

        DataHeuristik tanjungpriok_heu = new DataHeuristik("Tanjung Priok", tanjungpriok_hash);
        tanjungpriok_heu.setKecamatan("Tanjung Priok");
        DataHeuristikRepo.saveAndFlush(tanjungpriok_heu);


        Map<String, Double> koja_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 25.43);
            put("Pasarminggu", 17.25);
            put("Cilandak", 22.71);
            put("Pesanggrahan", 22.12);
            put("Kebayoran Lama", 20.47);
            put("Kebayoran Baru", 18.33);
            put("Mampang Prapatan", 17.50);
            put("Pancoran", 17.16);
            put("Tebet", 13.20);
            put("Setiabudi",14.63);
            put("Pasar Rebo", 23.81);
// --------------------------------
            put("Ciracas", 23.81);
            put("Cipayung", 23.66);
            put("Makasar", 17.79);
            put("Kramat Jati", 18.01);
            put("Jatinegara", 12.82);
            put("Duren Sawit", 13.00);
            put("Cakung", 8.34);
            put("Pulogadung", 8.56);
            put("Matraman", 10.65);
            put("Tanah Abang", 14.56);
            put("Menteng", 11.96);
// --------------------------------
            put("Senen", 10.30);
            put("Johar Baru", 9.27);
            put("Cempaka Putih", 8.25);
            put("Kemayoran", 7.48);
            put("Sawah Besar", 9.20);
            put("Gambir", 11.66);
            put("Kembangan", 15.87);
            put("Kebon Jeruk", 17.36);
            put("Palmerah", 14.93);
            put("Grogol Petamburan", 14.16);
// --------------------------------
            put("Tambora", 12.20);
            put("Taman Sari", 10.33);
            put("Cengkareng", 18.43);
            put("Kalideres", 22.88);
            put("Penjaringan", 15.50);
            put("Pademangan", 8.77);
            put("Tanjung Priok", 4.24);
            put("Koja", 0.0);
            put("Kelapa Gading", 4.70);
            put("Cilincing", 4.27);
        }};

        DataHeuristik koja_heu = new DataHeuristik("Koja", koja_hash);
        koja_heu.setKecamatan("Koja");
        DataHeuristikRepo.saveAndFlush(koja_heu);


        Map<String, Double> kelapagading_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 21.00);
            put("Pasarminggu", 12.83);
            put("Cilandak", 18.74);
            put("Pesanggrahan", 18.96);
            put("Kebayoran Lama", 17.22);
            put("Kebayoran Baru", 14.86);
            put("Mampang Prapatan", 13.57);
            put("Pancoran", 12.88);
            put("Tebet", 8.95);
            put("Setiabudi",11.08);
            put("Pasar Rebo", 19.19);
// --------------------------------
            put("Ciracas", 19.09);
            put("Cipayung", 18.91);
            put("Makasar", 13.11);
            put("Kramat Jati", 13.37);
            put("Jatinegara", 8.24);
            put("Duren Sawit", 8.34);
            put("Cakung", 4.75);
            put("Pulogadung", 3.87);
            put("Matraman", 6.59);
            put("Tanah Abang", 11.72);
            put("Menteng", 8.96);
// --------------------------------
            put("Senen", 7.39);
            put("Johar Baru", 6.08);
            put("Cempaka Putih", 4.68);
            put("Kemayoran", 5.36);
            put("Sawah Besar", 7.95);
            put("Gambir", 9.87);
            put("Kembangan", 18.55);
            put("Kebon Jeruk", 15.42);
            put("Palmerah", 12.74);
            put("Grogol Petamburan", 12.95);
// --------------------------------
            put("Tambora", 11.64);
            put("Taman Sari", 9.74);
            put("Cengkareng", 17.79);
            put("Kalideres", 22.70);
            put("Penjaringan", 16.00);
            put("Pademangan", 9.14);
            put("Tanjung Priok", 5.19);
            put("Koja", 4.42);
            put("Kelapa Gading", 0.0);
            put("Cilincing", 5.56);
        }};

        DataHeuristik kelapagading_heu = new DataHeuristik("Kelapa Gading", kelapagading_hash);
        kelapagading_heu.setKecamatan("Kelapa Gading");
        DataHeuristikRepo.saveAndFlush(kelapagading_heu);

        Map<String, Double> cilincing_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 27.14);
            put("Pasarminggu", 19.04);
            put("Cilandak", 25.13);
            put("Pesanggrahan", 25.28);
            put("Kebayoran Lama", 23.56);
            put("Kebayoran Baru", 21.24);
            put("Mampang Prapatan", 19.98);
            put("Pancoran", 19.20);
            put("Tebet", 15.31);
            put("Setiabudi",17.47);
            put("Pasar Rebo", 25.05);
// --------------------------------
            put("Ciracas", 24.60);
            put("Cipayung", 24.12);
            put("Makasar", 18.89);
            put("Kramat Jati", 19.26);
            put("Jatinegara", 14.37);
            put("Duren Sawit", 13.08);
            put("Cakung", 7.36);
            put("Pulogadung", 9.22);
            put("Matraman", 13.00);
            put("Tanah Abang", 17.88);
            put("Menteng", 15.15);
// --------------------------------
            put("Senen", 13.52);
            put("Johar Baru", 12.29);
            put("Cempaka Putih", 10.99);
            put("Kemayoran", 10.97);
            put("Sawah Besar", 13.10);
            put("Gambir", 15.43);
            put("Kembangan", 24.11);
            put("Kebon Jeruk", 21.15);
            put("Palmerah", 18.58);
            put("Grogol Petamburan", 18.16);
// --------------------------------
            put("Tambora", 16.34);
            put("Taman Sari", 14.44);
            put("Cengkareng", 22.60);
            put("Kalideres", 27.14);
            put("Penjaringan", 19.79);
            put("Pademangan", 13.02);
            put("Tanjung Priok", 8.41);
            put("Koja", 4.30);
            put("Kelapa Gading", 8.70);
            put("Cilincing", 0.0);
        }};

        DataHeuristik cilincing_heu = new DataHeuristik("Cilincing", cilincing_hash);
        cilincing_heu.setKecamatan("Cilincing");
        DataHeuristikRepo.saveAndFlush(cilincing_heu);

//TetanggaAsli===================================================================
        String[] tambora_tet = {"Penjaringan","Grogol Petamburan","Taman Sari","Gambir", "Pademangan"};
        Map<String, Double> tambora_tetHash = new HashMap<String, Double>(){{
            put("Penjaringan", 4.00);
            put("Grogol Petamburan", 2.80);
            put("Taman Sari", 2.70);
            put("Gambir", 3.80);
            put("Pademangan", 7.30);
        }};
        DataTetanggaAsli tambora_tetangga = new DataTetanggaAsli("Tambora", tambora_tet, tambora_tetHash);
        tambora_tetangga.setKecamatan("Tambora");
        DataTetanggaAsliRepo.saveAndFlush(tambora_tetangga);

        String[] tamansari_tet = {"Pademangan","Tambora","Gambir","Sawah Besar"};
        Map<String, Double> tamansari_tetHash = new HashMap<String, Double>(){{
            put("Tambora", 3.30);
            put("Pademangan", 4.90);
            put("Gambir", 4.70);
            put("Sawah Besar", 2.90);
        }};
        DataTetanggaAsli tamansari_tetangga = new DataTetanggaAsli("Taman Sari", tamansari_tet, tamansari_tetHash);
        tamansari_tetangga.setKecamatan("Taman Sari");
        DataTetanggaAsliRepo.saveAndFlush(tamansari_tetangga);

        String[] cengkareng_tet = {"Kalideres","Penjaringan","Kembangan","Kebon Jeruk","Grogol Petamburan"};
        Map<String, Double> cengkareng_tetHash = new HashMap<String, Double>(){{
            put("Kalideres", 7.60);
            put("Penjaringan", 13.0);
            put("Kembangan", 9.70);
            put("Kebon Jeruk", 9.30);
            put("Grogol Petamburan", 8.20);
        }};
        DataTetanggaAsli cengkareng_tetangga = new DataTetanggaAsli("Cengkareng", cengkareng_tet, cengkareng_tetHash);
        cengkareng_tetangga.setKecamatan("Cengkareng");
        DataTetanggaAsliRepo.saveAndFlush(cengkareng_tetangga);

        String[] kalideres_tet = {"Cengkareng","Penjaringan"};
        Map<String, Double> kalideres_tetHash = new HashMap<String, Double>(){{
            put("Cengkareng", 6.60);
            put("Penjaringan", 17.90);
        }};
        DataTetanggaAsli kalideres_tetangga = new DataTetanggaAsli("Kalideres", kalideres_tet, kalideres_tetHash);
        kalideres_tetangga.setKecamatan("Kalideres");
        DataTetanggaAsliRepo.saveAndFlush(kalideres_tetangga);

        String[] penjaringan_tet = {"Cengkareng","Kalideres","Grogol Petamburan","Pademangan","Tambora","Taman Sari"};
        Map<String, Double> penjaringan_tetHash = new HashMap<String, Double>(){{
            put("Cengkareng", 14.0);
            put("Kalideres", 17.90);
            put("Grogol Petamburan", 5.80);
            put("Pademangan", 6.10);
            put("Tambora", 5.10);
            put("Taman Sari", 4.70);
        }};
        DataTetanggaAsli penjaringan_tetangga = new DataTetanggaAsli("Penjaringan", penjaringan_tet, penjaringan_tetHash);
        penjaringan_tetangga.setKecamatan("Penjaringan");
        DataTetanggaAsliRepo.saveAndFlush(penjaringan_tetangga);

        String[] pademangan_tet = {"Penjaringan","Taman Sari","Tambora","Sawah Besar","Kemayoran","Tanjung Priok"};
        Map<String, Double> pademangan_tetHash = new HashMap<String, Double>(){{
            put("Penjaringan", 4.80);
            put("Taman Sari", 4.70);
            put("Tambora", 6.70);
            put("Sawah Besar", 3.70);
            put("Kemayoran", 8.80);
            put("Tanjung Priok", 6.60);
        }};
        DataTetanggaAsli pademangan_tetangga = new DataTetanggaAsli("Pademangan", pademangan_tet, pademangan_tetHash);
        pademangan_tetangga.setKecamatan("Pademangan");
        DataTetanggaAsliRepo.saveAndFlush(pademangan_tetangga);

        String[] tanjungpriok_tet = {"Pademangan","Kemayoran","Koja","Kelapa Gading"};
        Map<String, Double> tanjungpriok_tetHash = new HashMap<String, Double>(){{
            put("Pademangan", 5.00);
            put("Kemayoran", 7.80);
            put("Koja", 8.0);
            put("Kelapa Gading", 6.10);
        }};
        DataTetanggaAsli tanjungpriok_tetangga = new DataTetanggaAsli("Tanjung Priok", tanjungpriok_tet, tanjungpriok_tetHash);
        tanjungpriok_tetangga.setKecamatan("Tanjung Priok");
        DataTetanggaAsliRepo.saveAndFlush(tanjungpriok_tetangga);

        String[] koja_tet = {"Kelapa Gading","Tanjung Priok","Cilincing"};
        Map<String, Double> koja_tetHash = new HashMap<String, Double>(){{
            put("Kelapa Gading", 6.90);
            put("Tanjung Priok", 6.70);
            put("Cilincing", 8.10);
        }};
        DataTetanggaAsli koja_tetangga = new DataTetanggaAsli("Koja", koja_tet, koja_tetHash);
        koja_tetangga.setKecamatan("Koja");
        DataTetanggaAsliRepo.saveAndFlush(koja_tetangga);

        String[] kelapagading_tet = {"Koja","Tanjung Priok","Cilincing","Kemayoran","Johar Baru","Pulogadung","Cakung"};
        Map<String, Double> kelapagading_tetHash = new HashMap<String, Double>(){{
            put("Koja", 7.10);
            put("Tanjung Priok", 8.70);
            put("Cilincing", 11.20);
            put("Kemayoran", 9.90);
            put("Johar Baru", 8.90);
            put("Pulogadung", 4.20);
            put("Cakung", 7.80);
        }};
        DataTetanggaAsli kelapagading_tetangga = new DataTetanggaAsli("Kelapa Gading", kelapagading_tet, kelapagading_tetHash);
        kelapagading_tetangga.setKecamatan("Kelapa Gading");
        DataTetanggaAsliRepo.saveAndFlush(kelapagading_tetangga);

        String[] cilincing_tet = {"Koja","Kelapa Gading","Cakung"};
        Map<String, Double> cilincing_tetHash = new HashMap<String, Double>(){{
            put("Koja", 7.20);
            put("Kelapa Gading", 10.2);
            put("Cakung", 12.10);
        }};
        DataTetanggaAsli cilincing_tetangga = new DataTetanggaAsli("Cilincing", cilincing_tet, cilincing_tetHash);
        cilincing_tetangga.setKecamatan("Cilincing");
        DataTetanggaAsliRepo.saveAndFlush(cilincing_tetangga);
    }
}
