package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataHeuristik;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.repository.DataHeuristikRepository;
import com.restuibu.saferoute.repository.DataTetanggaAsliRepository;
import com.restuibu.saferoute.repository.DataWilayahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DataInputTsaq implements DataInput{
    DataWilayahRepository DataWilayahRepo;
    DataTetanggaAsliRepository DataTetanggaAsliRepo;
    DataHeuristikRepository DataHeuristikRepo;

    @Autowired
    public DataInputTsaq(
            DataWilayahRepository dataWilayahRepository,
            DataTetanggaAsliRepository dataTetanggaAsliRepository,
            DataHeuristikRepository dataHeuristikRepository) {
        DataWilayahRepo = dataWilayahRepository;
        DataTetanggaAsliRepo = dataTetanggaAsliRepository;
        DataHeuristikRepo = dataHeuristikRepository;
    }

    @Override
    public void inputData() {
        // Populasi===================================================================
        DataWilayah ciracas = new DataWilayah("Ciracas", 306469L);
        ciracas.setKecamatan("Ciracas");
        DataWilayahRepo.saveAndFlush(ciracas);

        DataWilayah cipayung = new DataWilayah("Cipayung", 280975L);
        cipayung.setKecamatan("Cipayung");
        DataWilayahRepo.saveAndFlush(cipayung);

        DataWilayah makasar = new DataWilayah("Makasar", 219840L);
        makasar.setKecamatan("Makasar");
        DataWilayahRepo.saveAndFlush(makasar);

        DataWilayah kramatjati = new DataWilayah("Kramat Jati", 309873L);
        kramatjati.setKecamatan("Kramat Jati");
        DataWilayahRepo.saveAndFlush(kramatjati);

        DataWilayah jatinegara = new DataWilayah("Jatinegara", 309873L);
        jatinegara.setKecamatan("Jatinegara");
        DataWilayahRepo.saveAndFlush(jatinegara);

        DataWilayah durenSawit = new DataWilayah("Duren Sawit", 433993L);
        durenSawit.setKecamatan("Duren Sawit");
        DataWilayahRepo.saveAndFlush(durenSawit);

        DataWilayah cakung = new DataWilayah("Cakung", 555511L);
        cakung.setKecamatan("Cakung");
        DataWilayahRepo.saveAndFlush(cakung);

        DataWilayah pulogadung = new DataWilayah("Pulogadung", 302190L);
        pulogadung.setKecamatan("Pulogadung");
        DataWilayahRepo.saveAndFlush(pulogadung);

        DataWilayah matraman = new DataWilayah("Matraman", 187647L);
        matraman.setKecamatan("Matraman");
        DataWilayahRepo.saveAndFlush(matraman);

        DataWilayah tanahAbang = new DataWilayah("Tanah Abang", 178931L);
        tanahAbang.setKecamatan("Tanah Abang");
        DataWilayahRepo.saveAndFlush(tanahAbang);

        DataWilayah menteng = new DataWilayah("Menteng", 90151L);
        menteng.setKecamatan("Menteng");
        DataWilayahRepo.saveAndFlush(menteng);


// DataHeuristik===================================================================
        Map<String, Double> ciracas_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 5.38);
            put("Pasarminggu", 6.11);
            put("Cilandak", 8.94);
            put("Pesanggrahan", 14.69);
            put("Kebayoran Lama", 13.57);
            put("Kebayoran Baru", 11.79);
            put("Mampang Prapatan", 9.73);
            put("Pancoran", 8.23);
            put("Tebet", 10.49);
            put("Setiabudi", 12.24);
            put("Pasar Rebo", 1.75);
// --------------------------------
            put("Ciracas", 0.00);
            put("Cipayung", 3.28);
            put("Makasar", 6.39);
            put("Kramat Jati", 4.75);
            put("Jatinegara", 10.36);
            put("Duren Sawit", 11.26);
            put("Cakung", 17.81);
            put("Pulogadung", 14.55);
            put("Matraman", 13.37);
            put("Tanah Abang", 14.94);
            put("Menteng", 14.73);
// --------------------------------
            put("Senen", 14.67);
            put("Johar Baru", 15.28);
            put("Cempaka Putih", 15.66);
            put("Kemayoran", 18.24);
            put("Sawah Besar", 19.37);
            put("Gambir", 18.02);
            put("Kembangan", 20.28);
            put("Kebon Jeruk", 19.51);
            put("Palmerah", 16.88);
            put("Grogol Petamburan", 20.33);
// --------------------------------
            put("Tambora", 20.77);
            put("Taman Sari", 20.18);
            put("Cengkareng", 24.49);
            put("Kalideres", 27.89);
            put("Penjaringan", 22.80);
            put("Pademangan", 21.47);
            put("Tanjung Priok", 21.17);
            put("Koja", 23.11);
            put("Kelapa Gading", 18.52);
            put("Cilincing", 23.98);
        }};
        DataHeuristik ciracas_heu = new DataHeuristik("Ciracas", ciracas_hash);
        ciracas_heu.setKecamatan("Ciracas");
        DataHeuristikRepo.saveAndFlush(ciracas_heu);

        Map<String, Double> cipayung_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 8.53);
            put("Pasarminggu", 7.85);
            put("Cilandak", 12.07);
            put("Pesanggrahan", 17.70);
            put("Kebayoran Lama", 16.47);
            put("Kebayoran Baru", 14.43);
            put("Mampang Prapatan", 12.19);
            put("Pancoran", 10.18);
            put("Tebet", 12.12);
            put("Setiabudi", 14.08);
            put("Pasar Rebo", 4.86);
// --------------------------------
            put("Ciracas", 3.27);
            put("Cipayung", 0.00);
            put("Makasar", 6.25);
            put("Kramat Jati", 6.72);
            put("Jatinegara", 10.86);
            put("Duren Sawit", 10.61);
            put("Cakung", 16.81);
            put("Pulogadung", 14.84);
            put("Matraman", 14.40);
            put("Tanah Abang", 17.01);
            put("Menteng", 16.35);
// --------------------------------
            put("Senen", 15.83);
            put("Johar Baru", 16.25);
            put("Cempaka Putih", 16.48);
            put("Kemayoran", 19.36);
            put("Sawah Besar", 20.71);
            put("Gambir", 19.85);
            put("Kembangan", 22.93);
            put("Kebon Jeruk", 21.86);
            put("Palmerah", 18.94);
            put("Grogol Petamburan", 22.45);
// --------------------------------
            put("Tambora", 22.51);
            put("Taman Sari", 21.71);
            put("Cengkareng", 27.02);
            put("Kalideres", 30.55);
            put("Penjaringan", 24.60);
            put("Pademangan", 22.64);
            put("Tanjung Priok", 21.89);
            put("Koja", 23.26);
            put("Kelapa Gading", 18.42);
            put("Cilincing", 14.49);
        }};
        DataHeuristik cipayung_heu = new DataHeuristik("Cipayung", cipayung_hash);
        cipayung_heu.setKecamatan("Cipayung");
        DataHeuristikRepo.saveAndFlush(cipayung_heu);

        Map<String, Double> makasar_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 10.58);
            put("Pasarminggu", 6.37);
            put("Cilandak", 10.59);
            put("Pesanggrahan", 14.91);
            put("Kebayoran Lama", 13.39);
            put("Kebayoran Baru", 10.87);
            put("Mampang Prapatan", 8.51);
            put("Pancoran", 5.64);
            put("Tebet", 6.83);
            put("Setiabudi", 8.97);
            put("Pasar Rebo", 7.44);
// --------------------------------
            put("Ciracas", 6.35);
            put("Cipayung", 6.26);
            put("Makasar", 0.00);
            put("Kramat Jati", 4.12);
            put("Jatinegara", 4.68);
            put("Duren Sawit", 4.88);
            put("Cakung", 11.42);
            put("Pulogadung", 8.62);
            put("Matraman", 8.34);
            put("Tanah Abang", 11.95);
            put("Menteng", 10.74);
// --------------------------------
            put("Senen", 9.93);
            put("Johar Baru", 10.22);
            put("Cempaka Putih", 10.23);
            put("Kemayoran", 13.32);
            put("Sawah Besar", 14.76);
            put("Gambir", 14.24);
            put("Kembangan", 18.90);
            put("Kebon Jeruk", 17.34);
            put("Palmerah", 14.07);
            put("Grogol Petamburan", 17.20);
// --------------------------------
            put("Tambora", 16.90);
            put("Taman Sari", 9.84);
            put("Cengkareng", 22.14);
            put("Kalideres", 25.81);
            put("Penjaringan", 18.74);
            put("Pademangan", 16.58);
            put("Tanjung Priok", 15.68);
            put("Koja", 17.09);
            put("Kelapa Gading", 12.33);
            put("Cilincing", 17.58);
        }};
        DataHeuristik makasar_heu = new DataHeuristik("Makasar", makasar_hash);
        makasar_heu.setKecamatan("Makasar");
        DataHeuristikRepo.saveAndFlush(makasar_heu);

        Map<String, Double> kramatJati_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 6.96);
            put("Pasarminggu", 2.23);
            put("Cilandak", 6.49);
            put("Pesanggrahan", 11.38);
            put("Kebayoran Lama", 10.03);
            put("Kebayoran Baru", 7.82);
            put("Mampang Prapatan", 5.51);
            put("Pancoran", 3.61);
            put("Tebet", 5.79);
            put("Setiabudi", 7.61);
            put("Pasar Rebo", 4.85);
// --------------------------------
            put("Ciracas", 4.68);
            put("Cipayung", 6.76);
            put("Makasar", 4.14);
            put("Kramat Jati", 0.00);
            put("Jatinegara", 6.34);
            put("Duren Sawit", 8.37);
            put("Cakung", 14.85);
            put("Pulogadung", 10.47);
            put("Matraman", 8.85);
            put("Tanah Abang", 10.33);
            put("Menteng", 10.04);
// --------------------------------
            put("Senen", 10.00);
            put("Johar Baru", 10.66);
            put("Cempaka Putih", 11.17);
            put("Kemayoran", 13.62);
            put("Sawah Besar", 14.70);
            put("Gambir", 13.39);
            put("Kembangan", 16.25);
            put("Kebon Jeruk", 15.15);
            put("Palmerah", 12.32);
            put("Grogol Petamburan", 15.78);
// --------------------------------
            put("Tambora", 16.12);
            put("Taman Sari", 15.46);
            put("Cengkareng", 20.13);
            put("Kalideres", 23.68);
            put("Penjaringan", 18.25);
            put("Pademangan", 16.79);
            put("Tanjung Priok", 16.79);
            put("Koja", 19.07);
            put("Kelapa Gading", 14.50);
            put("Cilincing", 20.43);
        }};
        DataHeuristik kramatJati_heu = new DataHeuristik("Kramat Jati", kramatJati_hash);
        kramatJati_heu.setKecamatan("Kramat Jati");
        DataHeuristikRepo.saveAndFlush(kramatJati_heu);

        Map<String, Double> jatinegara_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 13.32);
            put("Pasarminggu", 8.08);
            put("Cilandak", 10.93);
            put("Pesanggrahan", 13.46);
            put("Kebayoran Lama", 11.83);
            put("Kebayoran Baru", 9.25);
            put("Mampang Prapatan", 7.18);
            put("Pancoran", 4.56);
            put("Tebet", 3.88);
            put("Setiabudi", 5.65);
            put("Pasar Rebo", 11.03);
// --------------------------------
            put("Ciracas", 10.38);
            put("Cipayung", 10.92);
            put("Makasar", 4.69);
            put("Kramat Jati", 6.32);
            put("Jatinegara", 0.00);
            put("Duren Sawit", 3.61);
            put("Cakung", 8.95);
            put("Pulogadung", 4.22);
            put("Matraman", 3.80);
            put("Tanah Abang", 8.41);
            put("Menteng", 6.67);
// --------------------------------
            put("Senen", 5.47);
            put("Johar Baru", 5.60);
            put("Cempaka Putih", 5.60);
            put("Kemayoran", 8.73);
            put("Sawah Besar", 10.21);
            put("Gambir", 10.00);
            put("Kembangan", 16.11);
            put("Kebon Jeruk", 14.03);
            put("Palmerah", 10.46);
            put("Grogol Petamburan", 13.25);
// --------------------------------
            put("Tambora", 12.66);
            put("Taman Sari", 11.40);
            put("Cengkareng", 18.69);
            put("Kalideres", 22.23);
            put("Penjaringan", 14.30);
            put("Pademangan", 11.95);
            put("Tanjung Priok", 11.02);
            put("Koja", 12.81);
            put("Kelapa Gading", 8.22);
            put("Cilincing", 14.07);
        }};
        DataHeuristik jatinegara_heu = new DataHeuristik("Jatinegara", jatinegara_hash);
        jatinegara_heu.setKecamatan("Jatinegara");
        DataHeuristikRepo.saveAndFlush(jatinegara_heu);

        Map<String, Double> durenSawit_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 15.24);
            put("Pasarminggu", 10.55);
            put("Cilandak", 13.95);
            put("Pesanggrahan", 17.07);
            put("Kebayoran Lama", 15.38);
            put("Kebayoran Baru", 12.76);
            put("Mampang Prapatan", 10.60);
            put("Pancoran", 7.81);
            put("Tebet", 7.50);
            put("Setiabudi", 9.22);
            put("Pasar Rebo", 12.34);
// --------------------------------
            put("Ciracas", 11.17);
            put("Cipayung", 10.67);
            put("Makasar", 4.88);
            put("Kramat Jati", 8.36);
            put("Jatinegara", 3.58);
            put("Duren Sawit", 0.00);
            put("Cakung", 6.58);
            put("Pulogadung", 5.02);
            put("Matraman", 6.71);
            put("Tanah Abang", 11.91);
            put("Menteng", 9.92);
// --------------------------------
            put("Senen", 8.37);
            put("Johar Baru", 8.10);
            put("Cempaka Putih", 7.59);
            put("Kemayoran", 10.92);
            put("Sawah Besar", 12.66);
            put("Gambir", 13.04);
            put("Kembangan", 19.50);
            put("Kebon Jeruk", 17.56);
            put("Palmerah", 13.84);
            put("Grogol Petamburan", 16.45);
// --------------------------------
            put("Tambora", 15.39);
            put("Taman Sari", 13.98);
            put("Cengkareng", 21.90);
            put("Kalideres", 25.57);
            put("Penjaringan", 16.90);
            put("Pademangan", 13.99);
            put("Tanjung Priok", 12.13);
            put("Koja", 12.71);
            put("Kelapa Gading", 8.05);
            put("Cilincing", 12.85);
        }};
        DataHeuristik durenSawit_heu = new DataHeuristik("Duren Sawit", durenSawit_hash);
        durenSawit_heu.setKecamatan("Duren Sawit");
        DataHeuristikRepo.saveAndFlush(durenSawit_heu);

        Map<String, Double> cakung_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 21.86);
            put("Pasarminggu", 16.89);
            put("Cilandak", 19.99);
            put("Pesanggrahan", 21.82);
            put("Kebayoran Lama", 20.13);
            put("Kebayoran Baru", 17.74);
            put("Mampang Prapatan", 15.93);
            put("Pancoran", 13.51);
            put("Tebet", 12.37);
            put("Setiabudi", 13.36);
            put("Pasar Rebo", 18.94);
// --------------------------------
            put("Ciracas", 17.83);
            put("Cipayung", 16.92);
            put("Makasar", 11.47);
            put("Kramat Jati", 14.90);
            put("Jatinegara", 8.98);
            put("Duren Sawit", 6.63);
            put("Cakung", 0.00);
            put("Pulogadung", 6.37);
            put("Matraman", 9.74);
            put("Tanah Abang", 15.15);
            put("Menteng", 12.77);
// --------------------------------
            put("Senen", 10.82);
            put("Johar Baru", 9.95);
            put("Cempaka Putih", 8.79);
            put("Kemayoran", 11.32);
            put("Sawah Besar", 13.12);
            put("Gambir", 14.75);
            put("Kembangan", 22.72);
            put("Kebon Jeruk", 20.22);
            put("Palmerah", 16.62);
            put("Grogol Petamburan", 18.14);
// --------------------------------
            put("Tambora", 16.46);
            put("Taman Sari", 14.50);
            put("Cengkareng", 23.86);
            put("Kalideres", 27.22);
            put("Penjaringan", 17.05);
            put("Pademangan", 13.27);
            put("Tanjung Priok", 10.11);
            put("Koja", 8.47);
            put("Kelapa Gading", 5.28);
            put("Cilincing", 6.75);
        }};
        DataHeuristik cakung_heu = new DataHeuristik("Cakung", cakung_hash);
        cakung_heu.setKecamatan("Cakung");
        DataHeuristikRepo.saveAndFlush(cakung_heu);

        Map<String, Double > pulogadung_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 17.34);
            put("Pasarminggu", 12.07);
            put("Cilandak", 14.25);
            put("Pesanggrahan", 15.52);
            put("Kebayoran Lama", 13.91);
            put("Kebayoran Baru", 11.53);
            put("Mampang Prapatan", 10.03);
            put("Pancoran", 8.12);
            put("Tebet", 6.46);
            put("Setiabudi", 7.10);
            put("Pasar Rebo", 15.21);
// --------------------------------
            put("Ciracas", 14.52);
            put("Cipayung", 14.83);
            put("Makasar", 8.58);
            put("Kramat Jati", 10.49);
            put("Jatinegara", 4.20);
            put("Duren Sawit", 5.05);
            put("Cakung", 6.35);
            put("Pulogadung", 0.00);
            put("Matraman", 3.39);
            put("Tanah Abang", 8.82);
            put("Menteng", 6.44);
// --------------------------------
            put("Senen", 4.54);
            put("Johar Baru", 3.79);
            put("Cempaka Putih", 2.85);
            put("Kemayoran", 6.07);
            put("Sawah Besar", 7.88);
            put("Gambir", 8.87);
            put("Kembangan", 16.45);
            put("Kebon Jeruk", 14.01);
            put("Palmerah", 10.40);
            put("Grogol Petamburan", 12.33);
// --------------------------------
            put("Tambora", 10.91);
            put("Taman Sari", 9.25);
            put("Cengkareng", 17.97);
            put("Kalideres", 21.52);
            put("Penjaringan", 12.10);
            put("Pademangan", 8.92);
            put("Tanjung Priok", 7.17);
            put("Koja", 8.62);
            put("Kelapa Gading", 4.00);
            put("Cilincing", 10.13);
        }};
        DataHeuristik pulogadung_heu = new DataHeuristik("Pulogadung", pulogadung_hash);
        pulogadung_heu.setKecamatan("Pulogadung");
        DataHeuristikRepo.saveAndFlush(pulogadung_heu);

        Map<String, Double> matraman_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 15.26);
            put("Pasarminggu", 9.92);
            put("Cilandak", 11.30);
            put("Pesanggrahan", 12.13);
            put("Kebayoran Lama", 10.50);
            put("Kebayoran Baru", 8.22);
            put("Mampang Prapatan", 6.99);
            put("Pancoran", 5.68);
            put("Tebet", 3.57);
            put("Setiabudi", 3.72);
            put("Pasar Rebo", 13.70);
// --------------------------------
            put("Ciracas", 13.35);
            put("Cipayung", 14.40);
            put("Makasar", 8.37);
            put("Kramat Jati", 8.84);
            put("Jatinegara", 3.80);
            put("Duren Sawit", 6.71);
            put("Cakung", 9.68);
            put("Pulogadung", 3.38);
            put("Matraman", 0.00);
            put("Tanah Abang", 5.56);
            put("Menteng", 3.23);
// --------------------------------
            put("Senen", 1.71);
            put("Johar Baru", 1.89);
            put("Cempaka Putih", 2.39);
            put("Kemayoran", 5.05);
            put("Sawah Besar", 6.47);
            put("Gambir", 6.38);
            put("Kembangan", 13.31);
            put("Kebon Jeruk", 11.02);
            put("Palmerah", 7.33);
            put("Grogol Petamburan", 9.74);
// --------------------------------
            put("Tambora", 8.84);
            put("Taman Sari", 7.59);
            put("Cengkareng", 15.29);
            put("Kalideres", 18.96);
            put("Penjaringan", 10.54);
            put("Pademangan", 8.26);
            put("Tanjung Priok", 8.00);
            put("Koja", 10.68);
            put("Kelapa Gading", 6.75);
            put("Cilincing", 13.14 );
        }};
        DataHeuristik matraman_heu = new DataHeuristik("Matraman", matraman_hash);
        matraman_heu.setKecamatan("Matraman");
        DataHeuristikRepo.saveAndFlush(matraman_heu);

        Map<String, Double> tanahAbang_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 14.88);
            put("Pasarminggu", 10.20);
            put("Cilandak", 9.28);
            put("Pesanggrahan", 7.51);
            put("Kebayoran Lama", 6.13);
            put("Kebayoran Baru", 4.77);
            put("Mampang Prapatan", 5.47);
            put("Pancoran", 6.82);
            put("Tebet", 5.13);
            put("Setiabudi", 2.99);
            put("Pasar Rebo", 14.58);
// --------------------------------
            put("Ciracas", 14.98);
            put("Cipayung", 16.99);
            put("Makasar", 11.95);
            put("Kramat Jati", 10.41);
            put("Jatinegara", 8.43);
            put("Duren Sawit", 11.89);
            put("Cakung", 15.13);
            put("Pulogadung", 8.83);
            put("Matraman", 5.51);
            put("Tanah Abang", 0.00);
            put("Menteng", 2.43);
// --------------------------------
            put("Senen", 4.36);
            put("Johar Baru", 5.33);
            put("Cempaka Putih", 6.56);
            put("Kemayoran", 6.09);
            put("Sawah Besar", 5.92);
            put("Gambir", 3.57);
            put("Kembangan", 7.72);
            put("Kebon Jeruk", 5.59);
            put("Palmerah", 2.11);
            put("Grogol Petamburan", 5.43);
// --------------------------------
            put("Tambora", 5.99);
            put("Taman Sari", 6.07);
            put("Cengkareng", 10.36);
            put("Kalideres", 13.96);
            put("Penjaringan", 8.25);
            put("Pademangan", 8.28);
            put("Tanjung Priok", 10.23);
            put("Koja", 14.00);
            put("Kelapa Gading", 11.35);
            put("Cilincing", 17.51);
        }};
        DataHeuristik tanahAbang_heu = new DataHeuristik("Tanah Abang", tanahAbang_hash);
        tanahAbang_heu.setKecamatan("Tanah Abang");
        DataHeuristikRepo.saveAndFlush(tanahAbang_heu);

        Map<String, Double> menteng_hash = new HashMap<String, Double>(){{
            put("Jagakarsa", 15.48);
            put("Pasarminggu", 10.46);
            put("Cilandak", 10.52);
            put("Pesanggrahan", 9.74);
            put("Kebayoran Lama", 8.22);
            put("Kebayoran Baru", 6.42);
            put("Mampang Prapatan", 6.21);
            put("Pancoran", 6.45);
            put("Tebet", 4.29);
            put("Setiabudi", 2.60);
            put("Pasar Rebo", 14.67);
// --------------------------------
            put("Ciracas", 14.75);
            put("Cipayung", 16.31);
            put("Makasar", 10.82);
            put("Kramat Jati", 10.01);
            put("Jatinegara", 6.67);
            put("Duren Sawit", 9.94);
            put("Cakung", 12.76);
            put("Pulogadung", 6.42);
            put("Matraman", 3.30);
            put("Tanah Abang", 2.44);
            put("Menteng", 0.00);
// --------------------------------
            put("Senen", 1.93);
            put("Johar Baru", 2.90);
            put("Cempaka Putih", 4.13);
            put("Kemayoran", 4.28);
            put("Sawah Besar", 4.77);
            put("Gambir", 3.52);
            put("Kembangan", 10.00);
            put("Kebon Jeruk", 7.68);
            put("Palmerah", 4.03);
            put("Grogol Petamburan", 6.59);
// --------------------------------
            put("Tambora", 6.20);
            put("Taman Sari", 5.47);
            put("Cengkareng", 12.04);
            put("Kalideres", 15.66);
            put("Penjaringan", 8.18);
            put("Pademangan", 7.08);
            put("Tanjung Priok", 8.29);
            put("Koja", 11.90);
            put("Kelapa Gading", 8.91);
            put("Cilincing", 15.14);
        }};
        DataHeuristik menteng_heu = new DataHeuristik("Menteng", menteng_hash);
        menteng_heu.setKecamatan("Menteng");
        DataHeuristikRepo.saveAndFlush(menteng_heu);

//TetanggaAsli===================================================================
        String[] ciracas_tet = {"Cipayung","Pasar Rebo","Kramat Jati"};
        Map<String, Double> ciracas_tetHash = new HashMap<String, Double>(){{
            put("Cipayung", 5.50);
            put("Pasar Rebo", 2.80);
            put("Kramat Jati", 7.40);
        }};
        DataTetanggaAsli ciracas_tetangga = new DataTetanggaAsli("Ciracas", ciracas_tet, ciracas_tetHash);
        ciracas_tetangga.setKecamatan("Ciracas");
        DataTetanggaAsliRepo.saveAndFlush(ciracas_tetangga);

        String[] cipayung_tet = {"Ciracas","Makasar","Kramat Jati"};
        Map<String, Double> cipayung_tetHash = new HashMap<String, Double>(){{
            put("Ciracas", 5.50);
            put("Makasar", 10.70);
            put("Kramat Jati", 9.80);
        }};
        DataTetanggaAsli cipayung_tetangga = new DataTetanggaAsli("Cipayung", cipayung_tet, cipayung_tetHash);
        cipayung_tetangga.setKecamatan("Cipayung");
        DataTetanggaAsliRepo.saveAndFlush(cipayung_tetangga);

        String[] makasar_tet = {"Cipayung","Kramat Jati", "Jatinegara","Duren Sawit"};
        Map<String, Double> makasar_tetHash = new HashMap<String, Double>(){{
            put("Cipayung", 10.2);
            put("Kramat Jati", 6.90);
            put("Jatinegara", 9.40);
            put("Duren Sawit", 8.80);
        }};
        DataTetanggaAsli makasar_tetangga = new DataTetanggaAsli("Makasar", makasar_tet, makasar_tetHash);
        makasar_tetangga.setKecamatan("Makasar");
        DataTetanggaAsliRepo.saveAndFlush(makasar_tetangga);

        String[] kramatJati_tet = {"Pasarminggu","Pasar Rebo","Ciracas","Makasar","Cipayung","Pancoran","Tebet","Jatinegara"};
        Map<String, Double> kramatJati_tetHash = new HashMap<String, Double>(){{
            put("Pasarminggu", 8.80);
            put("Pasar Rebo", 6.70);
            put("Ciracas", 6.70);
            put("Makasar", 7.40);
            put("Cipayung", 10.50);
            put("Pancoran", 6.40);
            put("Tebet", 10.00);
            put("Jatinegara", 8.30);
        }};
        DataTetanggaAsli kramatJati_tetangga = new DataTetanggaAsli("Kramat Jati", kramatJati_tet, kramatJati_tetHash);
        kramatJati_tetangga.setKecamatan("Kramat Jati");
        DataTetanggaAsliRepo.saveAndFlush(kramatJati_tetangga);

        String[] jatinegara_tet = {"Duren Sawit","Pulogadung","Matraman","Tebet","Pancoran","Makasar","Kramat Jati"};
        Map<String, Double> jatinegara_tetHash = new HashMap<String, Double>(){{
            put("Duren Sawit", 5.20);
            put("Pulogadung", 8.70);
            put("Matraman", 7.70);
            put("Tebet", 6.90);
            put("Pancoran", 7.30);
            put("Makasar", 9.00);
            put("Kramat Jati", 8.30);
        }};
        DataTetanggaAsli jatinegara_tetangga = new DataTetanggaAsli("Jatinegara", jatinegara_tet, jatinegara_tetHash);
        jatinegara_tetangga.setKecamatan("Jatinegara");
        DataTetanggaAsliRepo.saveAndFlush(jatinegara_tetangga);

        String[] durenSawit_tet = {"Cakung","Pulogadung","Jatinegara","Makasar"};
        Map<String, Double> durenSawit_tetHash = new HashMap<String, Double>(){{
            put("Cakung", 9.40);
            put("Pulogadung", 8.30);
            put("Jatinegara", 5.20);
            put("Makasar", 9.40);
        }};
        DataTetanggaAsli durenSawit_tetangga = new DataTetanggaAsli("Duren Sawit", durenSawit_tet, durenSawit_tetHash);
        durenSawit_tetangga.setKecamatan("Duren Sawit");
        DataTetanggaAsliRepo.saveAndFlush(durenSawit_tetangga);

        String[] cakung_tet = {"Duren Sawit","Pulogadung","Kelapa Gading","Cilincing"};
        Map<String, Double> cakung_tetHash = new HashMap<String, Double>(){{
            put("Duren Sawit", 9.90);
            put("Pulogadung", 8.00);
            put("Kelapa Gading", 7.90);
            put("Cilincing", 12.40);
        }};
        DataTetanggaAsli cakung_tetangga = new DataTetanggaAsli("Cakung", cakung_tet, cakung_tetHash);
        cakung_tetangga.setKecamatan("Cakung");
        DataTetanggaAsliRepo.saveAndFlush(cakung_tetangga);

        String[] pulogadung_tet = {"Cakung","Duren Sawit","Jatinegara","Matraman","Cempaka Putih","Kemayoran","Kelapa Gading"};
        Map<String, Double> pulogadung_tetHash = new HashMap<String, Double>(){{
            put("Cakung", 8.50);
            put("Duren Sawit", 8.90);
            put("Jatinegara", 7.50);
            put("Matraman", 4.70);
            put("Cempaka Putih", 3.90);
            put("Kemayoran", 9.70);
            put("Kelapa Gading", 4.30);
        }};
        DataTetanggaAsli pulogadung_tetangga = new DataTetanggaAsli("Pulogadung", pulogadung_tet, pulogadung_tetHash);
        pulogadung_tetangga.setKecamatan("Pulogadung");
        DataTetanggaAsliRepo.saveAndFlush(pulogadung_tetangga);

        String[] matraman_tet = {"Pulogadung","Jatinegara","Tebet","Menteng","Senen","Cempaka Putih"};
        Map<String, Double> matraman_tetHash = new HashMap<String, Double>(){{
            put("Pulogadung", 5.80);
            put("Jatinegara", 6.70);
            put("Tebet", 6.70);
            put("Menteng", 5.00);
            put("Senen", 3.70);
            put("Cempaka Putih", 5.30);
        }};
        DataTetanggaAsli matraman_tetangga = new DataTetanggaAsli("Matraman", matraman_tet, matraman_tetHash);
        matraman_tetangga.setKecamatan("Matraman");
        DataTetanggaAsliRepo.saveAndFlush(matraman_tetangga);

        String[] tanahAbang_tet = {"Setiabudi","Menteng","Kebayoran Baru","Kebayoran Lama","Palmerah","Gambir"};
        Map<String, Double> tanahAbang_tetHash = new HashMap<String, Double>(){{
            put("Setiabudi", 4.60);
            put("Menteng", 4.70);
            put("Kebayoran Baru", 7.80);
            put("Kebayoran Lama", 9.30);
            put("Palmerah", 9.40);
            put("Gambir", 5.20);
        }};
        DataTetanggaAsli tanahAbang_tetangga = new DataTetanggaAsli("Tanah Abang", tanahAbang_tet, tanahAbang_tetHash);
        tanahAbang_tetangga.setKecamatan("Tanah Abang");
        DataTetanggaAsliRepo.saveAndFlush(tanahAbang_tetangga);

        String[] menteng_tet = {"Senen","Gambir","Tanah Abang","Setiabudi","Tebet","Matraman"};
        Map<String, Double> menteng_tetHash = new HashMap<String, Double>(){{
            put("Senen", 5.00);
            put("Gambir", 5.60);
            put("Tanah Abang", 4.90);
            put("Setiabudi", 3.50);
            put("Tebet", 6.80);
            put("Matraman", 6.90);
        }};
        DataTetanggaAsli menteng_tetangga = new DataTetanggaAsli("Menteng", menteng_tet, menteng_tetHash);
        menteng_tetangga.setKecamatan("Menteng");
        DataTetanggaAsliRepo.saveAndFlush(menteng_tetangga);
    }
}
