package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataHeuristik;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.repository.DataHeuristikRepository;
import com.restuibu.saferoute.repository.DataTetanggaAsliRepository;
import com.restuibu.saferoute.repository.DataWilayahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static javax.swing.UIManager.put;

@Component
public class DataInputKyo implements DataInput {
    DataWilayahRepository DataWilayahRepo;
    DataTetanggaAsliRepository DataTetanggaAsliRepo;
    DataHeuristikRepository DataHeuristikRepo;

    @Autowired
    public DataInputKyo(
            DataWilayahRepository dataWilayahRepository,
            DataTetanggaAsliRepository dataTetanggaAsliRepository,
            DataHeuristikRepository dataHeuristikRepository) {
        DataWilayahRepo = dataWilayahRepository;
        DataTetanggaAsliRepo = dataTetanggaAsliRepository;
        DataHeuristikRepo = dataHeuristikRepository;
    }

    @Override
    public void inputData() {
        /* JAGAKARSA */
        String[] tetanggaJagakarsa = {"Pasar Rebo","Pasarminggu","Cilandak"};
        Map<String,Double> jarakKeTetanggaJagakarsa = new HashMap<String,Double>(){{
            put("Pasar Rebo",8.0);
            put("Pasarminggu",4.9);
            put("Cilandak",9.1);
        }};
        DataTetanggaAsli dataTetanggaJagakarsa = new DataTetanggaAsli("Jagakarsa", tetanggaJagakarsa,jarakKeTetanggaJagakarsa);
        dataTetanggaJagakarsa.setKecamatan("Jagakarsa");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaJagakarsa);

        Map<String, Double> nilaiHeuristikJagakarsa = new HashMap<String, Double>(){{
            put("Pasar Rebo",3.75);
            put("Pasarminggu",4.55);
            put("Cilandak",6.22);
            put("Pesanggrahan",11.91);
            put("Kebayoran Lama",11.35);
            put("Kebayoran Baru",10.48);
            put("Mampang Prapatan",9.36);
            put("Pancoran",9.53);
            put("Tebet",11.73);
            put("Setiatbudi",12.82);
            put("Pasar Rebo",3.75);
            put("Ciracas",5.36);
            put("Cipayung",7.3);
            put("Makasar",10.5);
            put("Kramat Jati",6.94);
            put("Jatinegara",13.0);
            put("Duren Sawit",15.19);
            put("Cakung",21.7);
            put("Pulogadung",17.37);
            put("Matraman",15.23);
            put("Tanah Abang",14.8);
            put("Menteng",15.1);
            put("Senen",18.01);
            put("Johar Baru",16.9);
            put("Cempaka Putih",17.66);
            put("Kemayoran",19.14);
            put("Sawah Besar",20.27);
            put("Gambir",18.32);
            put("Kembangan",18.18);
            put("Kebon Jeruk",16.4);
            put("Palmerah",15.28);
            put("Grogol Petamburan", 19.75);
            put("Tambora", 21.23);
            put("Taman Sari",20.74);
            put("Cengkareng",22.95);
            put("Kalideres",25.85);
            put("Penjaringan",23.06);
            put("Pademangan",22.56);
            put("Tanjung Priok", 23.2);
            put("Koja", 25.88);
            put("Kelapa Gading",21.4);
            put("Cilincing",27.42);
            put("Jagakarsa", 0.0);
        }};
        DataHeuristik JagakarsaDH = new DataHeuristik("Jagakarsa",nilaiHeuristikJagakarsa);
        JagakarsaDH.setKecamatan("Jagakarsa");
        DataHeuristikRepo.saveAndFlush(JagakarsaDH);

        DataWilayah JagakarsaDW = new DataWilayah("Jagakarsa", 390272);
        JagakarsaDW.setKecamatan("Jagakarsa");
        DataWilayahRepo.saveAndFlush(JagakarsaDW);
        /* END JAGAKARSA */

        /* Pasarminggu */
        String[] tetanggaPasarMinggu = {"Jagakarsa","Cilandak","Pancoran","Mampang Prapatan","Kramat Jati","Pasar Rebo"};
        Map<String,Double> jarakKeTetanggaPasarMinggu = new HashMap<String,Double>(){{
            put("Pasar Rebo",8.0);
            put("Pancoran",7.6);
            put("Cilandak",3.7);
            put("Jagakarsa",5.8);
            put("Mampang Prapatan",5.8);
            put("Kramat Jati",5.2);
        }};
        DataTetanggaAsli dataTetanggaPasarMinggu= new DataTetanggaAsli("Pasarminggu", tetanggaPasarMinggu,jarakKeTetanggaPasarMinggu);
        dataTetanggaPasarMinggu.setKecamatan("Pasarminggu");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaPasarMinggu);

        Map<String, Double> nilaiHeuristikPasarMinggu = new HashMap<String, Double>(){{
            put("Jagakarsa",4.55);
            put("Cilandak",2.83);
            put("Pesanggrahan",8.59);
            put("Kebayoran Lama",7.62);
            put("Kebayoran Baru",6.2);
            put("Mampang Prapatan",4.83);
            put("Pancoran",5.37);
            put("Tebet",7.45);
            put("Setiabudi",8.39);
            put("Pasar Rebo",5.05);
            put("Ciracas",6.14);
            put("Cipayung",8.39);
            put("Makasar",8.21);
            put("Kramat Jati",4.15);
            put("Jatinegara",9.65);
            put("Duren Sawit",12.27);
            put("Cakung",18.52);
            put("Pulogadung",13.49);
            put("Matraman",11.0);
            put("Tanah Abang",23.5);
            put("Menteng",10.65);
            put("Senen",10.84);
            put("Johar Baru",12.5);
            put("Cempaka Putih",13.32);
            put("Kemayoran",23.50);
            put("Sawah Besar",15.70);
            put("Gambir",13.81);
            put("Kembangan",14.4);
            put("Kebon Jeruk",12.27);
            put("Palmerah",10.94);
            put("Grogol Petamburan",15.36);
            put("Tambora",16.3);
            put("Taman Sari",16.27);
            put("Cengkareng",18.92);
            put("Kalideres",21.11);
            put("Penjaringan",18.55);
            put("Pademangan",18.04);
            put("Tanjung Priok",18.78);
            put("Koja",21.64);
            put("Kelapa Gading",17.38);
            put("Cilicing",23.5);
            put("Pasarminggu", 0.0);
        }};
        DataHeuristik PasarMingguDH = new DataHeuristik("Pasarminggu",nilaiHeuristikPasarMinggu);
        PasarMingguDH.setKecamatan("Pasarminggu");
        DataHeuristikRepo.saveAndFlush(PasarMingguDH);

        DataWilayah PasarMingguDW = new DataWilayah("Pasarminggu",307249);
        PasarMingguDW.setKecamatan("Pasarminggu");
        DataWilayahRepo.saveAndFlush(PasarMingguDW);

        /* END Pasarminggu */

        /* CILANDAK */
        String[] tetanggaCilandak = {"Kebayoran Baru","Kebayoran Lama","Mampang Prapatan","Jagakarsa","Pasarminggu"};
        Map<String,Double> jarakKeTetanggaCilandak = new HashMap<String,Double>(){{
            put("Kebayoran Baru",5.1);
            put("Kebayoran Lama",7.3);
            put("Mampang Prapatan",6.2);
            put("Jagakarsa",9.1);
            put("Pasarminggu",3.7);
        }};
        DataTetanggaAsli tetanggaCilandakDT = new DataTetanggaAsli("Cilandak",tetanggaCilandak,jarakKeTetanggaCilandak);
        tetanggaCilandakDT.setKecamatan("Cilandak");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaCilandakDT);

        Map<String, Double> nilaiHeuristikCilandak = new HashMap<String, Double>(){{
            put("Jagakarsa",6.22);
            put("Pasarminggu",2.83);
            put("Pesanggrahan",5.91);
            put("Kebayoran Lama",5.16);
            put("Kebayoran Baru",4.55);
            put("Mampang Prapatan",4.4);
            put("Pancoran",5.91);
            put("Tebet",7.82);
            put("Setiabudi",8.07);
            put("Pasar Rebo",7.7);
            put("Ciracas",8.91);
            put("Cipayung",11.16);
            put("Makasar",10.53);
            put("Kramat Jati",6.48);
            put("Jatinegara",10.9);
            put("Duren Sawit",13.97);
            put("Cakung",19.82);
            put("Pulogadung",14.35);
            put("Matraman",11.45);
            put("Tanah Abang",9.29);
            put("Menteng",10.23);
            put("Senen",11.5);
            put("Johar Baru",12.98);
            put("Cempaka Putih",13.66);
            put("Kemayoran",14.38);
            put("Sawah Besar",15.130);
            put("Gambir",12.88);
            put("Kembangan",12.04);
            put("Kebon Jeruk",10.35);
            put("Palmerah",9.32);
            put("Grogol Petamburan",13.81);
            put("Tambora",15.14);
            put("Taman Sari",15.37);
            put("Cengkareng",16.76);
            put("Kalideres",18.620);
            put("Penjaringan",17.39);
            put("Pademangan",17.55);
            put("Tanjung Priok",18.78);
            put("Koja",22.04);
            put("Kelapa Gading",18.13);
            put("Cilicing",24.47);
            put("Cilandak", 0.0);
        }};
        DataHeuristik CilandakDH = new DataHeuristik("Cilandak",nilaiHeuristikCilandak);
        CilandakDH.setKecamatan("Cilandak");
        DataHeuristikRepo.saveAndFlush(CilandakDH);

        DataWilayah CilandakDW = new DataWilayah("Cilandak", 201563);
        CilandakDW.setKecamatan("Cilandak");
        DataWilayahRepo.saveAndFlush(CilandakDW);
        /* END CILANDAK */

        /* PESANGGRAHAN */
        String[] tetanggaPesanggrahan = {"Kebayoran Lama","Kembangan","Kebon Jeruk"};
        Map<String,Double> jarakKeTetanggaPesanggrahan = new HashMap<String,Double>(){{
            put("Kebayoran Lama",3.0);
            put("Kembangan",9.3);
            put("Kebon Jeruk",8.8);
        }};
        DataTetanggaAsli tetanggaPesanggrahanDT = new DataTetanggaAsli("Pesanggrahan",tetanggaPesanggrahan,jarakKeTetanggaPesanggrahan);
        tetanggaPesanggrahanDT.setKecamatan("Pesanggrahan");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaPesanggrahanDT);

        Map<String, Double> nilaiHeuristikPesanggrahan = new HashMap<String, Double>(){{
            put("Jagakarsa",11.91);
            put("Pasarminggu",8.59);
            put("Cilandak",5.91);
            put("Kebayoran Lama",1.66);
            put("Kebayoran Baru",4.26);
            put("Mampang Prapatan",6.52);
            put("Pancoran",8.63);
            put("Tebet",9.62);
            put("Setiabudi",8.43);
            put("Pasar Rebo",13.61);
            put("Ciracas",14.7);
            put("Cipayung",16.96);
            put("Makasar",14.9);
            put("Kramat Jati",11.44);
            put("Jatinegara",13.5);
            put("Duren Sawit",17.05);
            put("Cakung",21.77);
            put("Pulogadung",15.49);
            put("Matraman",12.14);
            put("Tanah Abang",7.47);
            put("Menteng",9.4);
            put("Senen",11.48);
            put("Johar Baru",12.67);
            put("Cempaka Putih",13.77);
            put("Kemayoran",13.13);
            put("Sawah Besar",13.08);
            put("Gambir",10.38);
            put("Kembangan",6.42);
            put("Kebon Jeruk",5.88);
            put("Palmerah",7.47);
            put("Grogol Petamburan",9.87);
            put("Tambora",11.86);
            put("Taman Sari",12.8);
            put("Cengkareng",11.36);
            put("Kalideres",12.78);
            put("Penjaringan",14.0);
            put("Pademangan",15.33);
            put("Tanjung Priok",17.66);
            put("Koja",21.53);
            put("Kelapa Gading",18.59);
            put("Cilicing",24.86);
            put("Pesanggrahan", 0.0);
        }};
        DataHeuristik PesanggrahanDH = new DataHeuristik("Pesanggrahan",nilaiHeuristikPesanggrahan);
        PesanggrahanDH.setKecamatan("Pesanggrahan");
        DataHeuristikRepo.saveAndFlush(PesanggrahanDH);

        DataWilayah PesanggrahanDW = new DataWilayah("Pesanggrahan",  222522);
        PesanggrahanDW.setKecamatan("Pesanggrahan");
        DataWilayahRepo.saveAndFlush(PesanggrahanDW);
/* END PESANGGRAHAN

/* KEBAYORAN LAMA */
        String[] tetanggaKebayoranLama = {"Kebon Jeruk","Palmerah","Tanah Abang","Kebayoran Baru","Pesanggrahan","Cilandak"};
        Map<String,Double> jarakKeTetanggaKebayoranLama = new HashMap<String,Double>(){{
            put("Kebon Jeruk",6.6);
            put("Palmerah",7.4);
            put("Tanah Abang",7.6);
            put("Kebayoran Baru",3.8);
            put("Pesanggrahan",2.9);
            put("Cilandak",7.4);
        }};
        DataTetanggaAsli tetanggaKebayoranLamaDT = new DataTetanggaAsli("Kebayoran Lama",tetanggaKebayoranLama,jarakKeTetanggaKebayoranLama);
        tetanggaKebayoranLamaDT.setKecamatan("Kebayoran Lama");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaKebayoranLamaDT);

        Map<String, Double> nilaiHeuristikKebayoranLama = new HashMap<String, Double>(){{
            put("Jagakarsa",11.35);
            put("Pasarminggu",7.62);
            put("Cilandak",5.16);
            put("Pesanggrahan",1.66);
            put("Kebayoran Baru",2.6);
            put("Mampang Prapatan",4.93);
            put("Pancoran",7.02);
            put("Tebet",7.94);
            put("Setiabudi",6.79);
            put("Pasar Rebo",12.66);
            put("Ciracas",13.62);
            put("Cipayung",15.8);
            put("Makasar",13.39);
            put("Kramat Jati",10.07);
            put("Jatinegara",11.82);
            put("Duren Sawit",15.37);
            put("Cakung",20.1);
            put("Pulogadung",13.86);
            put("Matraman",10.48);
            put("Tanah Abang",6.08);
            put("Menteng",7.96);
            put("Senen",9.91);
            put("Johar Baru",11.12);
            put("Cempaka Putih",12.2);
            put("Kemayoran",11.74);
            put("Sawah Besar",11.86);
            put("Gambir",9.24);
            put("Kembangan",6.84);
            put("Kebon Jeruk",5.4);
            put("Palmerah",5.44);
            put("Grogol Petamburan",9.24);
            put("Tambora",10.99);
            put("Taman Sari",11.72);
            put("Cengkareng",11.59);
            put("Kalideres",13.5);
            put("Penjaringan",13.19);
            put("Pademangan",14.18);
            put("Tanjung Priok",16.3);
            put("Koja",20.09);
            put("Kelapa Gading",17.02);
            put("Cilicing",23.3);
            put("Kebayoran Lama", 0.0);
        }};
        DataHeuristik KebayoranLamaDH = new DataHeuristik("Kebayoran Lama",nilaiHeuristikKebayoranLama);
        KebayoranLamaDH.setKecamatan("Kebayoran Lama");
        DataHeuristikRepo.saveAndFlush(KebayoranLamaDH);

        DataWilayah KebayoranLamaDW = new DataWilayah("Kebayoran Lama",  307734);
        KebayoranLamaDW.setKecamatan("Kebayoran Lama");
        DataWilayahRepo.saveAndFlush(KebayoranLamaDW);
        /* END KEBAYORAN LAMA */

        /* KEBAYORAN BARU */
        String[] tetanggaKebayoranBaru = {"Kebayoran Lama","Tanah Abang","Setiabudi","Mampang Prapatan","Cilandak"};
        Map<String,Double> jarakKeTetanggaKebayoranBaru = new HashMap<String,Double>(){{
            put("Kebayoran Lama",3.8);
            put("Tanah Abang",7.0);
            put("Setiabudi",6.9);
            put("Mampang Prapatan",3.1);
            put("Cilandak",5.1);
        }};
        DataTetanggaAsli tetanggaKebayoranBaruDT = new DataTetanggaAsli("Kebayoran Baru",tetanggaKebayoranBaru,jarakKeTetanggaKebayoranBaru);
        tetanggaKebayoranBaruDT.setKecamatan("Kebayoran Baru");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaKebayoranBaruDT);

        Map<String, Double> nilaiHeuristikKebayoranBaru = new HashMap<String, Double>(){{
            put("Jagakarsa",11.35);
            put("Pasarminggu",7.62);
            put("Cilandak",5.16);
            put("Pesanggrahan",1.66);
            put("Kebayoran Lama",2.6);
            put("Mampang Prapatan",4.93);
            put("Pancoran",7.02);
            put("Tebet",7.94);
            put("Setiabudi",6.79);
            put("Pasar Rebo",12.66);
            put("Ciracas",13.62);
            put("Cipayung",15.8);
            put("Makasar",13.39);
            put("Kramat Jati",10.07);
            put("Jatinegara",11.82);
            put("Duren Sawit",15.37);
            put("Cakung",20.1);
            put("Pulogadung",13.86);
            put("Matraman",10.48);
            put("Tanah Abang",6.08);
            put("Menteng",7.96);
            put("Senen",9.91);
            put("Johar Baru",11.12);
            put("Cempaka Putih",12.2);
            put("Kemayoran",11.74);
            put("Sawah Besar",11.86);
            put("Gambir",9.24);
            put("Kembangan",6.84);
            put("Kebon Jeruk",5.4);
            put("Palmerah",5.44);
            put("Grogol Petamburan",9.24);
            put("Tambora",10.99);
            put("Taman Sari",11.72);
            put("Cengkareng",11.59);
            put("Kalideres",13.5);
            put("Penjaringan",13.19);
            put("Pademangan",14.18);
            put("Tanjung Priok",16.3);
            put("Koja",20.09);
            put("Kelapa Gading",17.02);
            put("Cilicing",23.3);
            put("Kebayoran Baru", 0.0);
        }};
        DataHeuristik KebayoranBaruDH = new DataHeuristik("Kebayoran Baru",nilaiHeuristikKebayoranBaru);
        KebayoranBaruDH.setKecamatan("Kebayoran Baru");
        DataHeuristikRepo.saveAndFlush(KebayoranBaruDH);

        DataWilayah KebayoranBaruDW = new DataWilayah("Kebayoran Baru",  307734);
        KebayoranBaruDW.setKecamatan("Kebayoran Baru");
        DataWilayahRepo.saveAndFlush(KebayoranBaruDW);

        /* END KEBAYORAN BARU */

        /* MAMPANG PRAPATAN */
        String[] tetanggaMampangPrapatan = {"Cilandak","Pasarminggu","Pancoran","Setiabudi","Tebet","Kebayoran Baru"};
        Map<String,Double> jarakKeTetanggaMampangPrapatan = new HashMap<String,Double>(){{
            put("Cilandak",6.6);
            put("Pasarminggu",7.0);
            put("Pancoran",6.6);
            put("Setiabudi",7.9);
            put("Tebet",7.6);
            put("Kebayoran Baru",4.2);
        }};
        DataTetanggaAsli tetanggaMampangPrapatanDT = new DataTetanggaAsli("Mampang Prapatan",tetanggaMampangPrapatan,jarakKeTetanggaMampangPrapatan);
        tetanggaMampangPrapatanDT.setKecamatan("Mampang Prapatan");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaMampangPrapatanDT);


        Map<String, Double> nilaiHeuristikMampangPrapatan = new HashMap<String, Double>(){{
            put("Jagakarsa",9.38);
            put("Pasarminggu",4.89);
            put("Cilandak",4.4);
            put("Pesanggrahan",6.52);
            put("Kebayoran Baru",2.42);
            put("Kebayoran Lama",4.93);
            put("Pancoran",2.13);
            put("Tebet",3.6);
            put("Setiabudi",3.69);
            put("Pasar Rebo",9.26);
            put("Ciracas",9.79);
            put("Cipayung",11.78);
            put("Makasar",8.47);
            put("Kramat Jati",5.52);
            put("Jatinegara",7.2);
            put("Duren Sawit",10.65);
            put("Cakung",15.93);
            put("Pulogadung",10.04);
            put("Matraman",6.97);
            put("Tanah Abang",5.44);
            put("Menteng",5.87);
            put("Senen",7.15);
            put("Johar Baru",8.47);
            put("Cempaka Putih",9.18);
            put("Kemayoran",10.46);
            put("Sawah Besar",10.91);
            put("Gambir",8.96);
            put("Kembangan",10.76);
            put("Kebon Jeruk",8.0);
            put("Palmerah",7.21);
            put("Grogol Petamburan",10.62);
            put("Tambora",11.42);
            put("Taman Sari",11.35);
            put("Cengkareng",14.74);
            put("Kalideres",17.47);
            put("Penjaringan",13.7);
            put("Pademangan",13.26);
            put("Tanjung Priok",14.33);
            put("Koja",17.55);
            put("Kelapa Gading",13.7);
            put("Cilicing",20.1);
            put("Mampang Prapatan", 0.0);
        }};
        DataHeuristik MampangPrapatanDH = new DataHeuristik("Mampang Prapatan",nilaiHeuristikMampangPrapatan);
        MampangPrapatanDH.setKecamatan("Mampang Prapatan");
        DataHeuristikRepo.saveAndFlush(MampangPrapatanDH);

        DataWilayah MampangPrapatanDW = new DataWilayah("Mampang Prapatan",146741);
        MampangPrapatanDW.setKecamatan("Mampang Prapatan");
        DataWilayahRepo.saveAndFlush(MampangPrapatanDW);

        /*END MAMPANG PRAPATAN */

        /* PANCORAN */
        String[] tetanggaPancoran = {"Mampang Prapatan","Tebet","Kramat Jati","Pasarminggu"};
        Map<String,Double> jarakKeTetanggaPancoran = new HashMap<String,Double>(){{
            put("Mampang Prapatan",4.9);
            put("Tebet",3.6);
            put("Kramat Jati",6.7);
            put("Pasarminggu",7.4);
        }};
        DataTetanggaAsli tetanggaPancoranDT = new DataTetanggaAsli("Pancoran",tetanggaPancoran,jarakKeTetanggaPancoran);
        tetanggaPancoranDT.setKecamatan("Pancoran");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaPancoranDT);


        Map<String, Double> nilaiHeuristikPancoran = new HashMap<String, Double>(){{
            put("Jagakarsa",9.53);
            put("Pasarminggu",5.37);
            put("Cilandak",5.91);
            put("Pesanggrahan",8.63);
            put("Mampang Prapatan",2.13);
            put("Kebayoran Baru",4.45);
            put("Kebayoran Lama",7.02);
            put("Tebet",2.26);
            put("Setiabudi",3.98);
            put("Pasar Rebo",8.29);
            put("Ciracas",8.31);
            put("Cipayung",9.98);
            put("Makasar",5.62);
            put("Kramat Jati",3.62);
            put("Jatinegara",4.56);
            put("Duren Sawit",7.8);
            put("Cakung",13.53);
            put("Pulogadung",8.0);
            put("Matraman",5.67);
            put("Tanah Abang",6.8);
            put("Menteng",6.12);
            put("Senen",6.53);
            put("Johar Baru",7.75);
            put("Cempaka Putih",8.04);
            put("Kemayoran",9.76);
            put("Sawah Besar",11.09);
            put("Gambir",9.78);
            put("Kembangan",13.34);
            put("Kebon Jeruk",10.26);
            put("Palmerah",8.86);
            put("Grogol Petamburan",12.23);
            put("Tambora",12.49);
            put("Taman Sari",11.85);
            put("Cengkareng",16.88);
            put("Kalideres",20.03);
            put("Penjaringan",14.61);
            put("Pademangan",13.32);
            put("Tanjung Priok",13.62);
            put("Koja",16.29);
            put("Kelapa Gading",12.04);
            put("Cilicing",18.27);
            put("Pancoran", 0.0);
        }};
        DataHeuristik PancoranDH = new DataHeuristik("Pancoran",nilaiHeuristikPancoran);
        PancoranDH.setKecamatan("Pancoran");
        DataHeuristikRepo.saveAndFlush(PancoranDH);

        DataWilayah PancoranDW = new DataWilayah("Pancoran",154693);
        PancoranDW.setKecamatan("Pancoran");
        DataWilayahRepo.saveAndFlush(PancoranDW);

        /*END PANCORAN */

        /* TEBET */
        String[] tetanggaTebet = {"Setiabudi","Jatinegara","Matraman","Menteng","Pancoran","Mampang Prapatan","Kramat Jati"};
        Map<String,Double> jarakKeTetanggaTebet = new HashMap<String,Double>(){{
            put("Mampang Prapatan",5.3);
            put("Pancoran",3.3);
            put("Kramat Jati",9.2);
            put("Jatinegara",7.5);
            put("Matraman",7.4);
            put("Menteng",5.3);
            put("Setiabudi",4.4);

        }};
        DataTetanggaAsli tetanggaTebetDT = new DataTetanggaAsli("Tebet",tetanggaTebet,jarakKeTetanggaTebet);
        tetanggaTebetDT.setKecamatan("Tebet");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaTebetDT);

        Map<String, Double> nilaiHeuristikTebet = new HashMap<String, Double>(){{
            put("Jagakarsa",11.73);
            put("Pasarminggu",7.45);
            put("Cilandak",7.82);
            put("Pesanggrahan",9.62);
            put("Mampang Prapatan",3.6);
            put("Kebayoran Baru",5.38);
            put("Kebayoran Lama",7.94);
            put("Pancoran",2.26);
            put("Setiabudi",2.26);
            put("Pasar Rebo",10.61);
            put("Ciracas",10.51);
            put("Cipayung",12.06);
            put("Makasar",6.81);
            put("Kramat Jati",5.81);
            put("Jatinegara",3.9);
            put("Duren Sawit",7.49);
            put("Cakung",12.35);
            put("Pulogadung",6.43);
            put("Matraman",3.57);
            put("Tanah Abang",5.12);
            put("Menteng",3.98);
            put("Senen",4.28);
            put("Johar Baru",5.51);
            put("Cempaka Putih",5.93);
            put("Kemayoran",7.52);
            put("Sawah Besar",8.91);
            put("Gambir",7.73);
            put("Kembangan",12.38);
            put("Kebon Jeruk",9.1);
            put("Palmerah",7.27);
            put("Grogol Petamburan",10.48);
            put("Tambora",10.45);
            put("Taman Sari",9.73);
            put("Cengkareng",15.46);
            put("Kalideres",18.8);
            put("Penjaringan",12.54);
            put("Pademangan",11.07);
            put("Tanjung Priok",11.41);
            put("Koja",14.3);
            put("Kelapa Gading",10.2);
            put("Cilicing",16.54);
            put("Tebet", 0.0);
        }};
        DataHeuristik TebetDH = new DataHeuristik("Tebet",nilaiHeuristikTebet);
        TebetDH.setKecamatan("Tebet");
        DataHeuristikRepo.saveAndFlush(TebetDH);

        DataWilayah TebetDW = new DataWilayah("Tebet",211287);
        TebetDW.setKecamatan("Tebet");
        DataWilayahRepo.saveAndFlush(TebetDW);
        /* END TEBET */

        /* SETIABUDI */
        String[] tetanggaSetiabudi = {"Tebet","Tanah Abang","Menteng","Kebayoran Baru","Mampang Prapatan"};
        Map<String,Double> jarakKeTetanggaSetiabudi = new HashMap<String,Double>(){{
            put("Mampang Prapatan",4.7);
            put("Tebet",4.1);
            put("Tanah Abang",6.2);
            put("Menteng",3.6);
            put("Kebayoran Baru",6.1);
        }};
        DataTetanggaAsli tetanggaSetiabudiDT = new DataTetanggaAsli("Setiabudi",tetanggaSetiabudi,jarakKeTetanggaSetiabudi);
        tetanggaSetiabudiDT.setKecamatan("Setiabudi");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaSetiabudiDT);

        Map<String, Double> nilaiHeuristikSetiabudi = new HashMap<String, Double>(){{
            put("Jagakarsa",12.82);
            put("Pasarminggu",8.39);
            put("Cilandak",8.07);
            put("Pesanggrahan",8.43);
            put("Mampang Prapatan",3.69);
            put("Kebayoran Baru",4.5);
            put("Kebayoran Lama",6.79);
            put("Pancoran",3.98);
            put("Tebet",2.26);
            put("Pasar Rebo",12.1);
            put("Ciracas",12.26);
            put("Cipayung",13.96);
            put("Makasar",8.91);
            put("Kramat Jati",7.57);
            put("Jatinegara",5.65);
            put("Duren Sawit",9.23);
            put("Cakung",13.35);
            put("Pulogadung",7.08);
            put("Matraman",3.71);
            put("Tanah Abang",3.02);
            put("Menteng",2.28);
            put("Senen",3.5);
            put("Johar Baru",4.82);
            put("Cempaka Putih",5.66);
            put("Kemayoran",6.33);
            put("Sawah Besar",7.4);
            put("Gambir",5.85);
            put("Kembangan",10.41);
            put("Kebon Jeruk",7.08);
            put("Palmerah",5.12);
            put("Grogol Petamburan",8.38);
            put("Tambora",8.49);
            put("Taman Sari",8.04);
            put("Cengkareng",13.37);
            put("Kalideres",16.76);
            put("Penjaringan",10.65);
            put("Pademangan",9.71);
            put("Tanjung Priok",10.65);
            put("Koja",13.99);
            put("Kelapa Gading",10.38);
            put("Cilicing",16.74);
            put("Setiabudi", 0.0);
        }};
        DataHeuristik SetiabudiDH = new DataHeuristik("Setiabudi",nilaiHeuristikSetiabudi);
        SetiabudiDH.setKecamatan("Setiabudi");
        DataHeuristikRepo.saveAndFlush(SetiabudiDH);

        DataWilayah SetiabudiDW = new DataWilayah("Setiabudi",140985);
        SetiabudiDW.setKecamatan("Setiabudi");
        DataWilayahRepo.saveAndFlush(SetiabudiDW);
        /* END SETIABUDI */

        /* PASAR REBO */
        String[] tetanggaPasarRebo = {"Jagakarsa","Ciracas","Kramat Jati","Pasarminggu"};
        Map<String,Double> jarakKeTetanggaPasarRebo = new HashMap<String,Double>(){{
            put("Jagakarsa",11.0);
            put("Ciracas",3.1);
            put("Kramat Jati",7.0);
            put("Pasarminggu",8.4);
        }};
        DataTetanggaAsli tetanggaPasarReboDT = new DataTetanggaAsli("Pasar Rebo",tetanggaPasarRebo,jarakKeTetanggaPasarRebo);
        tetanggaPasarReboDT.setKecamatan("Pasar Rebo");
        DataTetanggaAsliRepo.saveAndFlush(tetanggaPasarReboDT);

        Map<String, Double> nilaiHeuristikPasarRebo = new HashMap<String, Double>(){{
            put("Jagakarsa",3.75);
            put("Pasarminggu",5.05);
            put("Cilandak",7.7);
            put("Pesanggrahan",13.62);
            put("Mampang Prapatan",9.26);
            put("Kebayoran Baru",11.09);
            put("Kebayoran Lama",12.66);
            put("Pancoran",8.29);
            put("Tebet",10.61);
            put("Setiabudi",12.1);
            put("Ciracas",1.58);
            put("Cipayung",4.89);
            put("Makasar",7.5);
            put("Kramat Jati",4.9);
            put("Jatinegara",11.06);
            put("Duren Sawit",12.38);
            put("Cakung",18.96);
            put("Pulogadung",15.3);
            put("Matraman",13.74);
            put("Tanah Abang",14.65);
            put("Menteng",14.4);
            put("Senen",14.78);
            put("Johar Baru",15.88);
            put("Cempaka Putih",16.01);
            put("Kemayoran",18.06);
            put("Sawah Besar",19.45);
            put("Gambir",17.89);
            put("Kembangan",19.48);
            put("Kebon Jeruk",17.1);
            put("Palmerah",16.45);
            put("Grogol Petamburan",19.95);
            put("Tambora",20.59);
            put("Taman Sari",20.21);
            put("Cengkareng",23.85);
            put("Kalideres",26.15);
            put("Penjaringan",23.78);
            put("Pademangan",21.56);
            put("Tanjung Priok",21.66);
            put("Koja",23.78);
            put("Kelapa Gading",19.19);
            put("Cilicing",24.94);
            put("Pasar Rebo", 0.0);
        }};
        DataHeuristik PasarReboDH = new DataHeuristik("Pasar Rebo",nilaiHeuristikPasarRebo);
        PasarReboDH.setKecamatan("Pasar Rebo");
        DataHeuristikRepo.saveAndFlush(PasarReboDH);

        DataWilayah PasarReboDW = new DataWilayah("Pasar Rebo",220000);
        PasarReboDW.setKecamatan("Pasar Rebo");
        DataWilayahRepo.saveAndFlush(PasarReboDW);
    }
}
