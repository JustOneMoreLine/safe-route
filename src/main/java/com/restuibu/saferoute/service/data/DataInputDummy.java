package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataHeuristik;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.repository.DataHeuristikRepository;
import com.restuibu.saferoute.repository.DataTetanggaAsliRepository;
import com.restuibu.saferoute.repository.DataWilayahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

//@Component
public class DataInputDummy implements DataInput {
    DataWilayahRepository DataWilayahRepo;
    DataTetanggaAsliRepository DataTetanggaAsliRepo;
    DataHeuristikRepository DataHeuristikRepo;

    @Autowired
    public DataInputDummy(
            DataWilayahRepository dataWilayahRepository,
            DataTetanggaAsliRepository dataTetanggaAsliRepository,
            DataHeuristikRepository dataHeuristikRepository) {
        DataWilayahRepo = dataWilayahRepository;
        DataTetanggaAsliRepo = dataTetanggaAsliRepository;
        DataHeuristikRepo = dataHeuristikRepository;
    }
    @Override
    public void inputData() {
        // format DataTetanggaAsli
        String[] tetangga = {"A","B"};
        Map<String, Double> jarakKeTetangga = new HashMap<String, Double>(){{
            put("A", 4.0);
            put("B", 2.0);
        }};
        DataTetanggaAsli jakartaDTA = new DataTetanggaAsli("Jakarta", tetangga, jarakKeTetangga);
        jakartaDTA.setKecamatan("Jakarta");
        DataTetanggaAsliRepo.saveAndFlush(jakartaDTA);

        String[] tetanggaA = {"Jakarta","B","C"};
        Map<String, Double> jarakKeTetanggaA = new HashMap<String, Double>(){{
            put("Jakarta", 4.0);
            put("B", 3.0);
            put("C", 4.0);
        }};
        DataTetanggaAsli aDTA = new DataTetanggaAsli("A", tetanggaA, jarakKeTetanggaA);
        aDTA.setKecamatan("A");
        DataTetanggaAsliRepo.saveAndFlush(aDTA);

        String[] tetanggaB = {"Jakarta","A","D"};
        Map<String, Double> jarakKeTetanggaB = new HashMap<String, Double>(){{
            put("Jakarta", 2.0);
            put("A", 3.0);
            put("D", 5.0);
        }};
        DataTetanggaAsli bDTA = new DataTetanggaAsli("B", tetanggaB, jarakKeTetanggaB);
        bDTA.setKecamatan("B");
        DataTetanggaAsliRepo.saveAndFlush(bDTA);

        String[] tetanggaC = {"A","Depok","D"};
        Map<String, Double> jarakKeTetanggaC = new HashMap<String, Double>(){{
            put("A", 4.0);
            put("D", 4.0);
            put("Depok", 3.0);
        }};
        DataTetanggaAsli cDTA = new DataTetanggaAsli("C", tetanggaC, jarakKeTetanggaC);
        cDTA.setKecamatan("C");
        DataTetanggaAsliRepo.saveAndFlush(cDTA);

        String[] tetanggaD = {"A","B", "Depok","C"};
        Map<String, Double> jarakKeTetanggaD = new HashMap<String, Double>(){{
            put("A", 5.0);
            put("B", 5.0);
            put("C", 4.0);
            put("Depok", 1.0);
        }};
        DataTetanggaAsli dDTA = new DataTetanggaAsli("D", tetanggaD, jarakKeTetanggaD);
        dDTA.setKecamatan("D");
        DataTetanggaAsliRepo.saveAndFlush(dDTA);

        String[] tetanggaDepok = {"D", "C"};
        Map<String, Double> jarakKeTetanggaDepok = new HashMap<String, Double>(){{
            put("C", 3.0);
            put("D", 1.0);
        }};
        DataTetanggaAsli depDTA = new DataTetanggaAsli("Depok", tetanggaDepok, jarakKeTetanggaDepok);
        depDTA.setKecamatan("Depok");
        DataTetanggaAsliRepo.saveAndFlush(depDTA);

        Map<String, Double> nilaiHeuristikKeJAK = new HashMap<String, Double>(){{
            put("Jakarta", 0.0);
            put("A", 3.0);
            put("B", 1.0);
            put("C", 7.0);
            put("D", 6.0);
            put("Depok", 7.0);
        }};
        DataHeuristik jak = new DataHeuristik("Jakarta", nilaiHeuristikKeJAK);
        jak.setKecamatan("Jakarta");
        DataHeuristikRepo.saveAndFlush(jak);

        Map<String, Double> anilaiHeuristikKe = new HashMap<String, Double>(){{
            put("Jakarta", 3.0);
            put("A", 0.0);
            put("B", 2.0);
            put("C", 3.0);
            put("D", 4.0);
            put("Depok", 5.0);
        }};
        DataHeuristik a = new DataHeuristik("A", anilaiHeuristikKe);
        a.setKecamatan("A");
        DataHeuristikRepo.saveAndFlush(a);

        Map<String, Double> bnilaiHeuristikKe = new HashMap<String, Double>(){{
            put("Jakarta", 1.0);
            put("A", 2.0);
            put("B", 0.0);
            put("C", 6.0);
            put("D", 4.0);
            put("Depok", 5.0);
        }};
        DataHeuristik b = new DataHeuristik("B", bnilaiHeuristikKe);
        b.setKecamatan("B");
        DataHeuristikRepo.saveAndFlush(b);

        Map<String, Double> cnilaiHeuristikKe = new HashMap<String, Double>(){{
            put("Jakarta", 7.0);
            put("A", 3.0);
            put("B", 6.0);
            put("C", 0.0);
            put("D", 3.0);
            put("Depok", 2.0);
        }};
        DataHeuristik c = new DataHeuristik("C", cnilaiHeuristikKe);
        c.setKecamatan("C");
        DataHeuristikRepo.saveAndFlush(c);

        Map<String, Double> dnilaiHeuristikKe = new HashMap<String, Double>(){{
            put("Jakarta", 6.0);
            put("A", 4.0);
            put("B", 4.0);
            put("C", 3.0);
            put("D", 0.0);
            put("Depok", 1.0);
        }};
        DataHeuristik d = new DataHeuristik("D", dnilaiHeuristikKe);
        d.setKecamatan("D");
        DataHeuristikRepo.saveAndFlush(d);

        Map<String, Double> depnilaiHeuristikKe = new HashMap<String, Double>(){{
            put("Jakarta", 7.0);
            put("A", 5.0);
            put("B", 5.0);
            put("C", 2.0);
            put("D", 1.0);
            put("Depok", 0.0);
        }};
        DataHeuristik dep = new DataHeuristik("Depok", depnilaiHeuristikKe);
        dep.setKecamatan("Depok");
        DataHeuristikRepo.saveAndFlush(dep);

        DataWilayah jak1 = new DataWilayah("Jakarta", 100L);
        jak1.setKecamatan("Jakarta");
        DataWilayahRepo.saveAndFlush(jak1);

        DataWilayah a1 = new DataWilayah("A", 100L);
        a1.setKecamatan("A");
        DataWilayahRepo.saveAndFlush(a1);

        DataWilayah a2 = new DataWilayah("B", 100L);
        a2.setKecamatan("B");
        DataWilayahRepo.saveAndFlush(a2);

        DataWilayah a3 = new DataWilayah("C", 100L);
        a3.setKecamatan("C");
        DataWilayahRepo.saveAndFlush(a3);

        DataWilayah a4 = new DataWilayah("D", 100L);
        a4.setKecamatan("D");
        DataWilayahRepo.saveAndFlush(a4);

        DataWilayah a5 = new DataWilayah("Depok", 100L);
        a5.setKecamatan("Depok");
        DataWilayahRepo.saveAndFlush(a5);
    }
}
