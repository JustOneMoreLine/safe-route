package com.restuibu.saferoute.service.data;

import com.restuibu.saferoute.core.DataHeuristik;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.repository.DataHeuristikRepository;
import com.restuibu.saferoute.repository.DataTetanggaAsliRepository;
import com.restuibu.saferoute.repository.DataWilayahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DataInputRifqi implements DataInput {
    DataWilayahRepository DataWilayahRepo;
    DataTetanggaAsliRepository DataTetanggaAsliRepo;
    DataHeuristikRepository DataHeuristikRepo;

    @Autowired
    public DataInputRifqi(
            DataWilayahRepository dataWilayahRepository,
            DataTetanggaAsliRepository dataTetanggaAsliRepository,
            DataHeuristikRepository dataHeuristikRepository) {
        DataWilayahRepo = dataWilayahRepository;
        DataTetanggaAsliRepo = dataTetanggaAsliRepository;
        DataHeuristikRepo = dataHeuristikRepository;
    }

    @Override
    public void inputData() {
        String[] tetanggaSenen = {"Cempaka Putih", "Johar Baru", "Matraman", "Menteng", "Gambir", "Sawah Besar", "Kemayoran"};
        Map<String, Double> jarakKeTetanggaSenen = new HashMap<String, Double>(){{
            put("Cempaka Putih", 3.3);
            put("Johar Baru", 1.8);
            put("Matraman", 3.6);
            put("Menteng", 3.2);
            put("Gambir", 6.8);
            put("Sawah Besar", 6.6);
            put("Kemayoran", 5.7);
        }};
        DataTetanggaAsli dataTetanggaSenen = new DataTetanggaAsli("Senen", tetanggaSenen, jarakKeTetanggaSenen);
        dataTetanggaSenen.setKecamatan("Senen");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaSenen);


        String[] tetanggaJoharBaru = {"Cempaka Putih", "Senen", "Kemayoran"};
        Map<String, Double> jarakKeTetanggaJoharBaru = new HashMap<String, Double>(){{
            put("Cempaka Putih", 3.2);
            put("Senen", 1.8);
            put("Kemayoran", 4.6);
        }};
        DataTetanggaAsli dataTetanggaJoharBaru = new DataTetanggaAsli("Johar Baru", tetanggaJoharBaru, jarakKeTetanggaJoharBaru);
        dataTetanggaJoharBaru.setKecamatan("Johar Baru");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaJoharBaru);


        String[] tetanggaCempakaPutih = {"Senen", "Johar Baru", "Kemayoran", "Tanjung Priok", "Kelapa Gading", "Pulogadung", "Matraman"};
        Map<String, Double> jarakKeTetanggaCempakaPutih = new HashMap<String, Double>(){{
            put("Senen", 4.4);
            put("Johar Baru", 2.6);
            put("Kemayoran", 5.2);
            put("Tanjung Priok", 10.2);
            put("Kelapa Gading", 7.8);
            put("Pulogadung", 6.1);
            put("Matraman", 6.7);
        }};
        DataTetanggaAsli dataTetanggaCempakaPutih = new DataTetanggaAsli("Cempaka Putih", tetanggaCempakaPutih, jarakKeTetanggaCempakaPutih);
        dataTetanggaCempakaPutih.setKecamatan("Cempaka Putih");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaCempakaPutih);


        String[] tetanggaKemayoran = {"Johar Baru", "Senen", "Sawah Besar", "Pademangan", "Tanjung Priok", "Kelapa Gading", "Pulogadung", "Cempaka Putih"};
        Map<String, Double> jarakKeTetanggaKemayoran = new HashMap<String, Double>(){{
            put("Johar Baru", 4.1);
            put("Senen", 5.4);
            put("Sawah Besar", 3.5);
            put("Pademangan", 5.5);
            put("Tanjung Priok", 6.8);
            put("Kelapa Gading", 9.8);
            put("Pulogadung", 9.9);
            put("Cempaka Putih", 7.5);
        }};
        DataTetanggaAsli dataTetanggaKemayoran = new DataTetanggaAsli("Kemayoran", tetanggaKemayoran, jarakKeTetanggaKemayoran);
        dataTetanggaKemayoran.setKecamatan("Kemayoran");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaKemayoran);


        String[] tetanggaSawahBesar = {"Senen", "Gambir", "Taman Sari", "Pademangan", "Kemayoran"};
        Map<String, Double> jarakKeTetanggaSawahBesar = new HashMap<String, Double>(){{
            put("Senen", 6.5);
            put("Gambir", 4.1);
            put("Taman Sari", 2.7);
            put("Pademangan", 4.4);
            put("Kemayoran", 3.3);
        }};
        DataTetanggaAsli dataTetanggaSawahBesar = new DataTetanggaAsli("Sawah Besar", tetanggaSawahBesar, jarakKeTetanggaSawahBesar);
        dataTetanggaSawahBesar.setKecamatan("Sawah Besar");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaSawahBesar);


        String[] tetanggaGambir = {"Menteng", "Tanah Abang", "Palmerah", "Grogol Petamburan", "Tambora", "Taman Sari", "Sawah Besar", "Senen"};
        Map<String, Double> jarakKeTetanggaGambir = new HashMap<String, Double>(){{
            put("Menteng", 6.4);
            put("Tanah Abang", 4.5);
            put("Palmerah", 6.2);
            put("Grogol Petamburan", 4.5);
            put("Tambora", 4.1);
            put("Taman Sari", 4.4);
            put("Sawah Besar", 4.3);
            put("Senen", 6.6);
        }};
        DataTetanggaAsli dataTetanggaGambir = new DataTetanggaAsli("Gambir", tetanggaGambir, jarakKeTetanggaGambir);
        dataTetanggaGambir.setKecamatan("Gambir");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaGambir);


        String[] tetanggaKembangan = {"Pesanggrahan", "Cengkareng", "Kebon Jeruk"};
        Map<String, Double> jarakKeTetanggaKembangan = new HashMap<String, Double>(){{
            put("Pesanggrahan", 9.0);
            put("Cengkareng", 8.2);
            put("Kebon Jeruk", 5.7);
        }};
        DataTetanggaAsli dataTetanggaKembangan = new DataTetanggaAsli("Kembangan", tetanggaKembangan, jarakKeTetanggaKembangan);
        dataTetanggaKembangan.setKecamatan("Kembangan");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaKembangan);


        String[] tetanggaKebonJeruk = {"Kebayoran Lama", "Pesanggrahan", "Kembangan", "Cengkareng", "Grogol Petamburan", "Palmerah"};
        Map<String, Double> jarakKeTetanggaKebonJeruk = new HashMap<String, Double>(){{
            put("Kebayoran Lama", 9.7);
            put("Pesanggrahan", 10.0);
            put("Kembangan", 5.9);
            put("Cengkareng", 10.4);
            put("Grogol Petamburan", 6.5);
            put("Palmerah", 6.9);
        }};
        DataTetanggaAsli dataTetanggaKebonJeruk = new DataTetanggaAsli("Kebon Jeruk", tetanggaKebonJeruk, jarakKeTetanggaKebonJeruk);
        dataTetanggaKebonJeruk.setKecamatan("Kebon Jeruk");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaKebonJeruk);


        String[] tetanggaPalmerah = {"Kebayoran Lama", "Kebon Jeruk", "Grogol Petamburan", "Gambir", "Tanah Abang"};
        Map<String, Double> jarakKeTetanggaPalmerah = new HashMap<String, Double>(){{
            put("Kebayoran Lama", 11.7);
            put("Kebon Jeruk", 13.2);
            put("Grogol Petamburan", 12.3);
            put("Gambir", 10.6);
            put("Tanah Abang", 5.4);
        }};
        DataTetanggaAsli dataTetanggaPalmerah = new DataTetanggaAsli("Palmerah", tetanggaPalmerah, jarakKeTetanggaPalmerah);
        dataTetanggaPalmerah.setKecamatan("Palmerah");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaPalmerah);


        String[] tetanggaGrogolPetamburan = {"Palmerah", "Kebon Jeruk", "Cengkareng", "Penjaringan", "Tambora", "Gambir"};
        Map<String, Double> jarakKeTetanggaGrogolPetamburan = new HashMap<String, Double>(){{
            put("Palmerah", 4.2);
            put("Kebon Jeruk", 7.0);
            put("Cengkareng", 9.0);
            put("Penjaringan", 8.0);
            put("Tambora", 4.6);
            put("Gambir", 4.9);
        }};
        DataTetanggaAsli dataTetanggaGrogolPetamburan = new DataTetanggaAsli("Grogol Petamburan", tetanggaGrogolPetamburan, jarakKeTetanggaGrogolPetamburan);
        dataTetanggaGrogolPetamburan.setKecamatan("Grogol Petamburan");
        DataTetanggaAsliRepo.saveAndFlush(dataTetanggaGrogolPetamburan);



//
// Bagian DataHeuristik Senen - Grogol Petamburan
//


        Map<String, Double> senenHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 16.09);
            put("Pasarminggu", 10.74);
            put("Cilandak", 11.55);
            put("Pesanggrahan", 11.47);
            put("Kebayoran Lama", 9.92);
            put("Kebayoran Baru", 7.89);
            put("Mampang Prapatan", 7.13);
            put("Pancoran", 6.55);
            put("Tebet", 4.32);
            put("Setiabudi", 3.5);
            put("Pasar Rebo", 14.75);
// --------------------------------
            put("Ciracas", 14.58);
            put("Cipayung", 15.87);
            put("Makasar", 9.94);
            put("Kramat Jati", 9.97);
            put("Jatinegara", 5.5);
            put("Duren Sawit", 8.39);
            put("Cakung", 10.88);
            put("Pulogadung", 4.54);
            put("Matraman", 1.72);
            put("Tanah Abang", 4.33);
            put("Menteng", 1.93);
// --------------------------------
            put("Senen", 0.0);
            put("Johar Baru", 1.33);
            put("Cempaka Putih", 2.3);
            put("Kemayoran", 3.68);
            put("Sawah Besar", 4.86);
            put("Gambir", 4.69);
            put("Kembangan", 11.88);
            put("Kebon Jeruk", 9.5);
            put("Palmerah", 5.86);
            put("Grogol Petamburan", 8.05);
// --------------------------------
            put("Tambora", 7.17);
            put("Taman Sari", 5.96);
            put("Cengkareng", 13.59);
            put("Kalideres", 17.23);
            put("Penjaringan", 8.88);
            put("Pademangan", 6.87);
            put("Tanjung Priok", 7.2);
            put("Koja", 10.44);
            put("Kelapa Gading", 7.13);
            put("Cilincing", 13.41);
        }};
        DataHeuristik senenHeuristic = new DataHeuristik("Senen", senenHeurHash);
        senenHeuristic.setKecamatan("Senen");
        DataHeuristikRepo.saveAndFlush(senenHeuristic);


        Map<String, Double> joharBaruHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 17.24);
            put("Pasarminggu", 11.93);
            put("Cilandak", 12.8);
            put("Pesanggrahan", 12.59);
            put("Kebayoran Lama", 11.13);
            put("Kebayoran Baru", 9.13);
            put("Mampang Prapatan", 8.41);
            put("Pancoran", 7.74);
            put("Tebet", 5.5);
            put("Setiabudi", 4.8);
            put("Pasar Rebo", 15.82);
// --------------------------------
            put("Ciracas", 15.65);
            put("Cipayung", 16.78);
            put("Makasar", 10.63);
            put("Kramat Jati", 11.02);
            put("Jatinegara", 6.09);
            put("Duren Sawit", 8.54);
            put("Cakung", 10.08);
            put("Pulogadung", 4.08);
            put("Matraman", 2.35);
            put("Tanah Abang", 5.32);
            put("Menteng", 2.92);
// --------------------------------
            put("Senen", 1.32);
            put("Johar Baru", 0.0);
            put("Cempaka Putih", 1.33);
            put("Kemayoran", 2.7);
            put("Sawah Besar", 4.2);
            put("Gambir", 4.78);
            put("Kembangan", 12.56);
            put("Kebon Jeruk", 10.08);
            put("Palmerah", 6.54);
            put("Grogol Petamburan", 8.22);
// --------------------------------
            put("Tambora", 7.01);
            put("Taman Sari", 5.46);
            put("Cengkareng", 13.84);
            put("Kalideres", 17.46);
            put("Penjaringan", 8.35);
            put("Pademangan", 5.9);
            put("Tanjung Priok", 5.93);
            put("Koja", 9.15);
            put("Kelapa Gading", 6.02);
            put("Cilincing", 12.21);
        }};
        DataHeuristik joharBaruHeuristic = new DataHeuristik("Johar Baru", joharBaruHeurHash);
        joharBaruHeuristic.setKecamatan("Johar Baru");
        DataHeuristikRepo.saveAndFlush(joharBaruHeuristic);


        Map<String, Double> cempakaPutihHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 17.61);
            put("Pasarminggu", 12.27);
            put("Cilandak", 13.55);
            put("Pesanggrahan", 13.78);
            put("Kebayoran Lama", 12.21);
            put("Kebayoran Baru", 10.1);
            put("Mampang Prapatan", 9.16);
            put("Pancoran", 8.04);
            put("Tebet", 5.92);
            put("Setiabudi", 5.67);
            put("Pasar Rebo", 15.93);
// --------------------------------
            put("Ciracas", 15.62);
            put("Cipayung", 16.49);
            put("Makasar", 10.29);
            put("Kramat Jati", 6.94);
            put("Jatinegara", 5.59);
            put("Duren Sawit", 7.55);
            put("Cakung", 8.82);
            put("Pulogadung", 2.82);
            put("Matraman", 2.38);
            put("Tanah Abang", 6.56);
            put("Menteng", 4.13);
// --------------------------------
            put("Senen", 2.3);
            put("Johar Baru", 1.3);
            put("Cempaka Putih", 0.0);
            put("Kemayoran", 3.38);
            put("Sawah Besar", 5.12);
            put("Gambir", 6.04);
            put("Kembangan", 13.91);
            put("Kebon Jeruk", 11.41);
            put("Palmerah", 7.86);
            put("Grogol Petamburan", 9.49);
// --------------------------------
            put("Tambora", 8.1);
            put("Taman Sari", 6.45);
            put("Cengkareng", 15.13);
            put("Kalideres", 18.71);
            put("Penjaringan", 9.31);
            put("Pademangan", 6.44);
            put("Tanjung Priok", 5.63);
            put("Koja", 8.39);
            put("Kelapa Gading", 4.82);
            put("Cilincing", 11.14);
        }};
        DataHeuristik cempakaPutihHeuristic = new DataHeuristik("Cempaka Putih", cempakaPutihHeurHash);
        cempakaPutihHeuristic.setKecamatan("Cempaka Putih");
        DataHeuristikRepo.saveAndFlush(cempakaPutihHeuristic);


        Map<String, Double> kemayoranHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 19.6);
            put("Pasarminggu", 14.4);
            put("Cilandak", 14.81);
            put("Pesanggrahan", 13.57);
            put("Kebayoran Lama", 12.18);
            put("Kebayoran Baru", 10.61);
            put("Mampang Prapatan", 10.45);
            put("Pancoran", 10.23);
            put("Tebet", 7.99);
            put("Setiabudi", 6.79);
            put("Pasar Rebo", 18.48);
// --------------------------------
            put("Ciracas", 18.29);
            put("Cipayung", 19.47);
            put("Makasar", 13.34);
            put("Kramat Jati", 13.64);
            put("Jatinegara", 8.8);
            put("Duren Sawit", 10.94);
            put("Cakung", 11.41);
            put("Pulogadung", 6.12);
            put("Matraman", 5.07);
            put("Tanah Abang", 6.1);
            put("Menteng", 4.31);
// --------------------------------
            put("Senen", 3.69);
            put("Johar Baru", 2.7);
            put("Cempaka Putih", 3.38);
            put("Kemayoran", 0.0);
            put("Sawah Besar", 1.82);
            put("Gambir", 3.77);
            put("Kembangan", 12.16);
            put("Kebon Jeruk", 9.45);
            put("Palmerah", 6.45);
            put("Grogol Petamburan", 6.91);
// --------------------------------
            put("Tambora", 5.02);
            put("Taman Sari", 3.2);
            put("Cengkareng", 12.48);
            put("Kalideres", 15.9);
            put("Penjaringan", 6.03);
            put("Pademangan", 3.2);
            put("Tanjung Priok", 4.11);
            put("Koja", 8.09);
            put("Kelapa Gading", 6.44);
            put("Cilincing", 11.97);
        }};
        DataHeuristik kemayoranHeuristic = new DataHeuristik("Kemayoran", kemayoranHeurHash);
        kemayoranHeuristic.setKecamatan("Kemayoran");
        DataHeuristikRepo.saveAndFlush(kemayoranHeuristic);


        Map<String, Double> sawahBesarHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 20.33);
            put("Pasarminggu", 15.18);
            put("Cilandak", 15.03);
            put("Pesanggrahan", 13.09);
            put("Kebayoran Lama", 11.93);
            put("Kebayoran Baru", 10.68);
            put("Mampang Prapatan", 10.93);
            put("Pancoran", 11.14);
            put("Tebet", 8.91);
            put("Setiabudi", 7.39);
            put("Pasar Rebo", 19.4);
// --------------------------------
            put("Ciracas", 19.44);
            put("Cipayung", 20.81);
            put("Makasar", 14.85);
            put("Kramat Jati", 14.69);
            put("Jatinegara", 10.26);
            put("Duren Sawit", 12.71);
            put("Cakung", 13.13);
            put("Pulogadung", 7.9);
            put("Matraman", 6.47);
            put("Tanah Abang", 5.93);
            put("Menteng", 4.78);
// --------------------------------
            put("Senen", 4.92);
            put("Johar Baru", 4.27);
            put("Cempaka Putih", 5.15);
            put("Kemayoran", 1.87);
            put("Sawah Besar", 0.0);
            put("Gambir", 2.74);
            put("Kembangan", 10.89);
            put("Kebon Jeruk", 8.2);
            put("Palmerah", 5.65);
            put("Grogol Petamburan", 5.35);
// --------------------------------
            put("Tambora", 3.25);
            put("Taman Sari", 1.37);
            put("Cengkareng", 10.77);
            put("Kalideres", 14.06);
            put("Penjaringan", 4.2);
            put("Pademangan", 2.37);
            put("Tanjung Priok", 4.92);
            put("Koja", 9.08);
            put("Kelapa Gading", 8.13);
            put("Cilincing", 13.24);
        }};
        DataHeuristik sawahBesarHeuristic = new DataHeuristik("Sawah Besar", sawahBesarHeurHash);
        sawahBesarHeuristic.setKecamatan("Sawah Besar");
        DataHeuristikRepo.saveAndFlush(sawahBesarHeuristic);


        Map<String, Double> gambirHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 18.32);
            put("Pasarminggu", 13.55);
            put("Cilandak", 12.81);
            put("Pesanggrahan", 10.4);
            put("Kebayoran Lama", 9.29);
            put("Kebayoran Baru", 8.35);
            put("Mampang Prapatan", 9.01);
            put("Pancoran", 9.81);
            put("Tebet", 7.76);
            put("Setiabudi", 5.84);
            put("Pasar Rebo", 17.91);
// --------------------------------
            put("Ciracas", 18.17);
            put("Cipayung", 19.8);
            put("Makasar", 14.28);
            put("Kramat Jati", 13.43);
            put("Jatinegara", 10.03);
            put("Duren Sawit", 13.07);
            put("Cakung", 14.73);
            put("Pulogadung", 8.82);
            put("Matraman", 6.38);
            put("Tanah Abang", 3.62);
            put("Menteng", 3.51);
// --------------------------------
            put("Senen", 4.69);
            put("Johar Baru", 4.77);
            put("Cempaka Putih", 6.01);
            put("Kemayoran", 3.73);
            put("Sawah Besar", 2.71);
            put("Gambir", 0.0);
            put("Kembangan", 8.41);
            put("Kebon Jeruk", 5.74);
            put("Palmerah", 2.96);
            put("Grogol Petamburan", 3.49);
// --------------------------------
            put("Tambora", 2.69);
            put("Taman Sari", 2.47);
            put("Cengkareng", 9.12);
            put("Kalideres", 12.7);
            put("Penjaringan", 4.78);
            put("Pademangan", 4.92);
            put("Tanjung Priok", 7.56);
            put("Koja", 11.66);
            put("Kelapa Gading", 10.09);
            put("Cilincing", 15.62);
        }};
        DataHeuristik gambirHeuristic = new DataHeuristik("Gambir", gambirHeurHash);
        gambirHeuristic.setKecamatan("Gambir");
        DataHeuristikRepo.saveAndFlush(gambirHeuristic);


        Map<String, Double> kembanganHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 18.14);
            put("Pasarminggu", 15.21);
            put("Cilandak", 12.02);
            put("Pesanggrahan", 6.44);
            put("Kebayoran Lama", 6.85);
            put("Kebayoran Baru", 8.5);
            put("Mampang Prapatan", 10.77);
            put("Pancoran", 13.3);
            put("Tebet", 12.4);
            put("Setiabudi", 10.4);
            put("Pasar Rebo", 19.44);
// --------------------------------
            put("Ciracas", 20.29);
            put("Cipayung", 22.92);
            put("Makasar", 18.94);
            put("Kramat Jati", 16.23);
            put("Jatinegara", 16.04);
            put("Duren Sawit", 19.57);
            put("Cakung", 22.7);
            put("Pulogadung", 16.42);
            put("Matraman", 13.22);
            put("Tanah Abang", 7.71);
            put("Menteng", 9.97);
// --------------------------------
            put("Senen", 11.89);
            put("Johar Baru", 12.6);
            put("Cempaka Putih", 13.94);
            put("Kemayoran", 12.16);
            put("Sawah Besar", 10.89);
            put("Gambir", 8.38);
            put("Kembangan", 0.0);
            put("Kebon Jeruk", 2.67);
            put("Palmerah", 6.04);
            put("Grogol Petamburan", 5.79);
// --------------------------------
            put("Tambora", 8.29);
            put("Taman Sari", 10.03);
            put("Cengkareng", 5.05);
            put("Kalideres", 7.72);
            put("Penjaringan", 9.85);
            put("Pademangan", 12.62);
            put("Tanjung Priok", 15.83);
            put("Koja", 19.96);
            put("Kelapa Gading", 18.42);
            put("Cilincing", 24.08);
        }};
        DataHeuristik kembanganHeuristic = new DataHeuristik("Kembangan", kembanganHeurHash);
        kembanganHeuristic.setKecamatan("Kembangan");
        DataHeuristikRepo.saveAndFlush(kembanganHeuristic);


        Map<String, Double> kebonJerukHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 18.03);
            put("Pasarminggu", 14.45);
            put("Cilandak", 11.91);
            put("Pesanggrahan", 7.18);
            put("Kebayoran Lama", 6.96);
            put("Kebayoran Baru", 7.77);
            put("Mampang Prapatan", 9.77);
            put("Pancoran", 11.93);
            put("Tebet", 10.63);
            put("Setiabudi", 8.57);
            put("Pasar Rebo", 18.84);
// --------------------------------
            put("Ciracas", 19.52);
            put("Cipayung", 21.97);
            put("Makasar", 17.45);
            put("Kramat Jati", 15.24);
            put("Jatinegara", 14.08);
            put("Duren Sawit", 17.6);
            put("Cakung", 20.28);
            put("Pulogadung", 14.04);
            put("Matraman", 11.0);
            put("Tanah Abang", 5.65);
            put("Menteng", 7.7);
// --------------------------------
            put("Senen", 9.56);
            put("Johar Baru", 10.17);
            put("Cempaka Putih", 11.41);
            put("Kemayoran", 9.53);
            put("Sawah Besar", 8.23);
            put("Gambir", 5.76);
            put("Kembangan", 2.62);
            put("Kebon Jeruk", 0.0);
            put("Palmerah", 3.66);
            put("Grogol Petamburan", 3.27);
// --------------------------------
            put("Tambora", 5.73);
            put("Taman Sari", 7.32);
            put("Cengkareng", 5.06);
            put("Kalideres", 8.51);
            put("Penjaringan", 7.44);
            put("Pademangan", 9.91);
            put("Tanjung Priok", 13.17);
            put("Koja", 17.29);
            put("Kelapa Gading", 15.79);
            put("Cilincing", 21.34);
        }};
        DataHeuristik kebonJerukHeuristic = new DataHeuristik("Kebon Jeruk", kebonJerukHeurHash);
        kebonJerukHeuristic.setKecamatan("Kebon Jeruk");
        DataHeuristikRepo.saveAndFlush(kebonJerukHeuristic);


        Map<String, Double> palmerahHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 16.41);
            put("Pasarminggu", 12.01);
            put("Cilandak", 10.51);
            put("Pesanggrahan", 7.51);
            put("Kebayoran Lama", 6.46);
            put("Kebayoran Baru", 5.94);
            put("Mampang Prapatan", 7.22);
            put("Pancoran", 8.82);
            put("Tebet", 7.21);
            put("Setiabudi", 5.08);
            put("Pasar Rebo", 16.48);
// --------------------------------
            put("Ciracas", 16.95);
            put("Cipayung", 19.0);
            put("Makasar", 14.02);
            put("Kramat Jati", 12.33);
            put("Jatinegara", 10.44);
            put("Duren Sawit", 13.8);
            put("Cakung", 16.64);
            put("Pulogadung", 10.33);
            put("Matraman", 7.28);
            put("Tanah Abang", 2.08);
            put("Menteng", 3.97);
// --------------------------------
            put("Senen", 5.81);
            put("Johar Baru", 6.53);
            put("Cempaka Putih", 7.81);
            put("Kemayoran", 6.4);
            put("Sawah Besar", 5.62);
            put("Gambir", 2.9);
            put("Kembangan", 6.1);
            put("Kebon Jeruk", 3.72);
            put("Palmerah", 0.0);
            put("Grogol Petamburan", 3.47);
// --------------------------------
            put("Tambora", 4.6);
            put("Taman Sari", 5.29);
            put("Cengkareng", 8.29);
            put("Kalideres", 11.91);
            put("Penjaringan", 6.85);
            put("Pademangan", 7.85);
            put("Tanjung Priok", 10.37);
            put("Koja", 14.45);
            put("Kelapa Gading", 12.35);
            put("Cilincing", 18.22);
        }};
        DataHeuristik palmerahHeuristic = new DataHeuristik("Palmerah", palmerahHeurHash);
        palmerahHeuristic.setKecamatan("Palmerah");
        DataHeuristikRepo.saveAndFlush(palmerahHeuristic);


        Map<String, Double> grogolPetamburanHeurHash = new HashMap<String, Double>(){{
            put("Jagakarsa", 19.78);
            put("Pasarminggu", 15.48);
            put("Cilandak", 13.78);
            put("Pesanggrahan", 9.86);
            put("Kebayoran Lama", 9.23);
            put("Kebayoran Baru", 9.26);
            put("Mampang Prapatan", 10.64);
            put("Pancoran", 12.23);
            put("Tebet", 10.45);
            put("Setiabudi", 8.35);
            put("Pasar Rebo", 19.96);
// --------------------------------
            put("Ciracas", 20.34);
            put("Cipayung", 22.45);
            put("Makasar", 17.16);
            put("Kramat Jati", 15.81);
            put("Jatinegara", 13.25);
            put("Duren Sawit", 16.48);
            put("Cakung", 18.22);
            put("Pulogadung", 12.38);
            put("Matraman", 9.77);
            put("Tanah Abang", 5.49);
            put("Menteng", 6.6);
// --------------------------------
            put("Senen", 8.11);
            put("Johar Baru", 8.27);
            put("Cempaka Putih", 9.52);
            put("Kemayoran", 6.94);
            put("Sawah Besar", 5.37);
            put("Gambir", 3.49);
            put("Kembangan", 5.85);
            put("Kebon Jeruk", 3.31);
            put("Palmerah", 3.5);
            put("Grogol Petamburan", 0.0);
// --------------------------------
            put("Tambora", 2.45);
            put("Taman Sari", 4.23);
            put("Cengkareng", 5.65);
            put("Kalideres", 9.21);
            put("Penjaringan", 4.21);
            put("Pademangan", 6.79);
            put("Tanjung Priok", 10.16);
            put("Koja", 14.34);
            put("Kelapa Gading", 13.37);
            put("Cilincing", 18.66);
        }};
        DataHeuristik grogolPetamburanHeuristic = new DataHeuristik("Grogol Petamburan", grogolPetamburanHeurHash);
        grogolPetamburanHeuristic.setKecamatan("Grogol Petamburan");
        DataHeuristikRepo.saveAndFlush(grogolPetamburanHeuristic);



//
// Bagian DataWilayah Senen - Grogol Petamburan
//


        DataWilayah senen = new DataWilayah("Senen", 97578L);
        senen.setKecamatan("Senen");
        DataWilayahRepo.saveAndFlush(senen);

        DataWilayah joharBaru = new DataWilayah("Johar Baru", 119994L);
        joharBaru.setKecamatan("Johar Baru");
        DataWilayahRepo.saveAndFlush(joharBaru);

        DataWilayah cempakaPutih = new DataWilayah("Cempaka Putih", 85667L);
        cempakaPutih.setKecamatan("Cempaka Putih");
        DataWilayahRepo.saveAndFlush(cempakaPutih);

        DataWilayah kemayoran = new DataWilayah("Kemayoran", 229175L);
        kemayoran.setKecamatan("Kemayoran");
        DataWilayahRepo.saveAndFlush(kemayoran);

        DataWilayah sawahBesar = new DataWilayah("Sawah Besar", 100958L);
        sawahBesar.setKecamatan("Sawah Besar");
        DataWilayahRepo.saveAndFlush(sawahBesar);

        DataWilayah gambir = new DataWilayah("Gambir", 78534L);
        gambir.setKecamatan("Gambir");
        DataWilayahRepo.saveAndFlush(gambir);

        DataWilayah kembangan = new DataWilayah("Kembangan", 334115L);
        kembangan.setKecamatan("Kembangan");
        DataWilayahRepo.saveAndFlush(kembangan);

        DataWilayah kebonJeruk = new DataWilayah("Kebon Jeruk", 383168L);
        kebonJeruk.setKecamatan("Kebon Jeruk");
        DataWilayahRepo.saveAndFlush(kebonJeruk);

        DataWilayah palmerah = new DataWilayah("Palmerah", 206353L);
        palmerah.setKecamatan("Palmerah");
        DataWilayahRepo.saveAndFlush(palmerah);

        DataWilayah grogolPetamburan = new DataWilayah("Grogol Petamburan", 241564L);
        grogolPetamburan.setKecamatan("Grogol Petamburan");
        DataWilayahRepo.saveAndFlush(grogolPetamburan);
    }
}
