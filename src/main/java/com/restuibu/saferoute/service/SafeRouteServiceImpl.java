package com.restuibu.saferoute.service;

//import com.restuibu.saferoute.core.Node;
import com.restuibu.saferoute.core.DataTetanggaAsli;
import com.restuibu.saferoute.core.Path;
//import com.restuibu.saferoute.core.Verteks;
//import com.restuibu.saferoute.repository.NodeRepository;
//import com.restuibu.saferoute.repository.VerteksRepository;
import com.restuibu.saferoute.service.comparator.heuristikComparator;
import com.restuibu.saferoute.service.comparator.heuristikComparator2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

/**
 * Logika pencarian A* Algo untuk Safe Route
 */
@Service
public class SafeRouteServiceImpl implements SafeRouteService {
    //private NodeRepository nodeRepository;
    //private VerteksRepository verteksRepository;
    private DatabaseService databaseService;

    @Autowired
    public SafeRouteServiceImpl(/*NodeRepository nodeRepository, VerteksRepository verteksRepository*/
            DatabaseService databaseService
    ) {
        //this.nodeRepository = nodeRepository;
        //this.verteksRepository = verteksRepository;
        this.databaseService = databaseService;
    }

    @Override
    public String shortRouteAtoB(String start, String goal) {
        Path initPath = new Path(start, goal);
        PriorityQueue<Path> open = new PriorityQueue<Path>(new heuristikComparator());
        ArrayList<Path> close = new ArrayList<Path>();
        open.add(initPath);
        while(!(open.isEmpty())) {
            Path currentPath = open.poll();
            close.add(currentPath);
            if(currentPath.getLast().equals(goal)) {
                System.out.println("SHORT==============");
                System.out.println("Jarak: " + currentPath.getJarak());
                for(String city : currentPath.getPath()) {
                    System.out.println(city + ": " + DatabaseServiceImpl.nilaiLevel(city));
                }
                System.out.println("===================");
                return currentPath.toString();
            }
            String lastNode = currentPath.getLast();
            for(String tetangga : databaseService.tetangga(lastNode)) {
                Path nextPath = currentPath.createNewPathWithThisCity(tetangga);
                if (close.contains(nextPath)) continue;
                if (open.contains(nextPath)) {
                    Path openPath = getThisPath(open, nextPath);
                    if (openPath.getJarak() < nextPath.getJarak()) continue;
                }
                open.add(nextPath);
            }
        }
        return null;
    }
    public String safeRouteAtoB(String start, String goal) {
        Path initPath = new Path(start, goal);
        PriorityQueue<Path> open = new PriorityQueue<Path>(new heuristikComparator2());
        ArrayList<Path> close = new ArrayList<Path>();
        open.add(initPath);
        while(!(open.isEmpty())) {
            Path currentPath = open.poll();
            close.add(currentPath);
            if(currentPath.getLast().equals(goal)) {
                System.out.println("SAFE==============");
                System.out.println("Jarak: " + currentPath.getJarak());
                for(String city : currentPath.getPath()) {
                    System.out.println(city + ": " + DatabaseServiceImpl.nilaiLevel(city));
                }
                System.out.println("===================");
                return currentPath.toString();
            }
            String lastNode = currentPath.getLast();
            for(String tetangga : databaseService.tetangga(lastNode)) {
                Path nextPath = currentPath.createNewPathWithThisCity(tetangga);
                if (close.contains(nextPath)) continue;
                if (open.contains(nextPath)) {
                    Path openPath = getThisPath(open, nextPath);
                    if (openPath.getJarak() < nextPath.getJarak()) continue;
                }
                open.add(nextPath);
            }
        }
        return null;
    }

    private Path getThisPath(PriorityQueue<Path> open, Path thisPath) {
        for(Path x : open) {
            if(x.equals(thisPath)) return x;
        }
        return null;
    }
}
