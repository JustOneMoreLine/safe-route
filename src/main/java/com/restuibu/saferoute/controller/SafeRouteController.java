package com.restuibu.saferoute.controller;

import com.restuibu.saferoute.core.DataWilayah;
import com.restuibu.saferoute.service.DatabaseService;
import com.restuibu.saferoute.service.SafeRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Controller
public class SafeRouteController {
    boolean dataInitYes = false;
    public SafeRouteService safeRouteService;
    public DatabaseService databaseService;

    @Autowired
    public SafeRouteController(SafeRouteService safeRouteService, DatabaseService databaseService) {
        this.safeRouteService = safeRouteService;
        this.databaseService = databaseService;
    }

    @GetMapping("/")
    public String home() {
        return "redirect:search";
    }

    @GetMapping("/search")
    public String searchPage(Model model) {
        List<DataWilayah> listOfKecamatan = databaseService.showAllKecamatan();
        model.addAttribute("listOfKecamatan", listOfKecamatan);
        return "search";
    }

    @PostMapping("/result")
    public String resultPage(Model model,
                             @RequestParam(value = "inputCurrentLocation") String currentLocation,
                             @RequestParam(value = "inputDestination") String destination) {
        String safestRoute = safeRouteService.safeRouteAtoB(currentLocation, destination);
        model.addAttribute("resultPath", safestRoute);
        model.addAttribute("start",currentLocation);
        model.addAttribute("destination", destination);
        return "result";
    }

    @PostMapping("/search")
    public String showResult(
            /*RedirectAttributes redirectAttributes,*/
            Model model,
            @RequestParam(value = "inputCurrentLocation") String currentLocation,
            @RequestParam(value = "inputDestination") String destination
    ) {
        String safestRoute = "A - B - C - D - E";
        String shortestRoute = "A - B - C - D - E";
        System.out.println(currentLocation);
        System.out.println(destination);
        shortestRoute = safeRouteService.shortRouteAtoB(currentLocation, destination);
        safestRoute = safeRouteService.safeRouteAtoB(currentLocation, destination);
        System.out.println("=================================================");
        System.out.println(safestRoute);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
        //redirectAttributes.addAttribute("result", safestRoute);
        model.addAttribute("resultPath1", shortestRoute);
        model.addAttribute("resultPath2", safestRoute);
        model.addAttribute("start",currentLocation);
        model.addAttribute("destination", destination);
        return "result";
    }

    @GetMapping("/test")
    public String testHome() {
        return "Hello World!";
    }

    /*@GetMapping("/data")
    public String test() throws FileNotFoundException, IOException {
        if(dataInitYes == false) {
            databaseService.inputIt();
            dataInitYes = true;
            databaseService.calculateCoronaLevel();
            System.out.println("DONE!");
        }
        return "MASUUUUUUUK";
    }*/
}
