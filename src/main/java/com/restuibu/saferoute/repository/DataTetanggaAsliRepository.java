package com.restuibu.saferoute.repository;

import com.restuibu.saferoute.core.DataTetanggaAsli;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataTetanggaAsliRepository extends JpaRepository<DataTetanggaAsli, String> {
}
