package com.restuibu.saferoute.repository;

import com.restuibu.saferoute.core.DataLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataLevelRepository extends JpaRepository<DataLevel, String> {
}
