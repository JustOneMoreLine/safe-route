package com.restuibu.saferoute.repository;

import com.restuibu.saferoute.core.DataCorona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DataCoronaRepository extends JpaRepository<DataCorona, Long> {
    public List<DataCorona> findAllByIdKecamatan(String idKecamatan);
    public List<DataCorona> findByIdKecamatan(String idKecamatan);
}
