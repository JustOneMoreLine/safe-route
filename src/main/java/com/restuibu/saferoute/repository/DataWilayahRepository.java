package com.restuibu.saferoute.repository;

import com.restuibu.saferoute.core.DataWilayah;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataWilayahRepository extends JpaRepository<DataWilayah, String> {
}
