package com.restuibu.saferoute.repository;

import com.restuibu.saferoute.core.DataHeuristik;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataHeuristikRepository extends JpaRepository<DataHeuristik, String> {
}
