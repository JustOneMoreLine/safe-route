package com.restuibu.saferoute.core;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Tabel database yang akan diisi oleh data-data murni
 * dari https://corona.jakarta.go.id/id/peta-persebaran.
 * Satu buah objek akan berisi sebuah data point dalam
 * sebuah bulan.
 */
@NoArgsConstructor
@Table(name = "datacorona")
@Entity
public class DataCorona {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    long idData; // id datapoint yang digenerate secara random, karena dalam 1 bulan bisa ada lebih dari 1 datapoint
    @NotNull
    @Column
    long bulan; // bulan apa datapoint diambil
    @NotNull
    @Column
    long tahun; // tahun apa datapoint diambil
    @NotNull
    @Column
    long tanggal; // tanggal apa datapoint diambil
    @NotNull
    @Column
    String idKecamatan;
    @Column
    long positif;
    @Column
    long dirawat;
    @Column
    long sembuh;
    @Column
    long meninggal;
    @Column
    long selfIsolation;

    public DataCorona(String idKecamatan,
                      long tanggal,
                      long bulan,
                      long tahun,
                      long positif, long dirawat, long sembuh, long meninggal, long selfIsolation) {
        this.idKecamatan = idKecamatan;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
        this.positif = positif;
        this.dirawat = dirawat;
        this.sembuh = sembuh;
        this.meninggal = meninggal;
        this.selfIsolation = selfIsolation;
    }

    /*public DataCorona(long idData,
                      long bulan,
                      long tahun,
                      long idKelurahan,
                      long positif,
                      long dirawat,
                      long sembuh,
                      long meninggal,
                      long selfIsolation) {
        this.idData = idData;
        this.bulan = bulan;
        this.tahun = tahun;
        this.idKecamatan = idKelurahan;
        this.positif = positif;
        this.dirawat = dirawat;
        this.sembuh = sembuh;
        this.meninggal = meninggal;
        this.selfIsolation = selfIsolation;
    }*/

    public long getTanggal() {
        return tanggal;
    }

    public void setTanggal(long tanggal) {
        this.tanggal = tanggal;
    }

    public void setIdData(long idData) {
        this.idData = idData;
    }

    public long getIdData() {
        return idData;
    }

    public void setBulan(long bulan) {
        this.bulan = bulan;
    }

    public long getBulan() {
        return bulan;
    }

    public void setTahun(long tahun) {
        this.tahun = tahun;
    }

    public long getTahun() {
        return tahun;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setPositif(long positif) {
        this.positif = positif;
    }

    public long getPositif() {
        return positif;
    }

    public long getDirawat() {
        return dirawat;
    }

    public void setDirawat(long dirawat) {
        this.dirawat = dirawat;
    }

    public long getSembuh() {
        return sembuh;
    }

    public void setSembuh(long sembuh) {
        this.sembuh = sembuh;
    }

    public long getMeninggal() {
        return meninggal;
    }

    public void setMeninggal(long meninggal) {
        this.meninggal = meninggal;
    }

    public long getSelfIsolation() {
        return selfIsolation;
    }

    public void setSelfIsolation(long selfIsolation) {
        this.selfIsolation = selfIsolation;
    }
}
