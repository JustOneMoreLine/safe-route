package com.restuibu.saferoute.core;

import jdk.internal.net.http.common.Pair;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Table(name = "datatetanggaasli")
@Entity
public class DataTetanggaAsli {
    @Id
    @Column(name = "id")
    String kecamatan;
    @Column
    String[] tetangga;

    @ElementCollection
    @CollectionTable(name = "kecamatan_jarak_map_asli",
            joinColumns = {@JoinColumn(name = "data_jarak_asli_map", referencedColumnName = "id")})
    @MapKeyColumn(name = "kecamatan_name")
    @Column(name = "jarak")
    Map<String, Double> jarakKeTetangga;

    public DataTetanggaAsli(String kecamatan, String[] tetangga, Map<String, Double> jarakKeTetangga) {
        this.kecamatan = kecamatan;
        this.tetangga = tetangga;
        this.jarakKeTetangga = jarakKeTetangga;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String[] getTetangga() {
        return tetangga;
    }

    public void setTetangga(String[] tetangga) {
        this.tetangga = tetangga;
    }

    public Map<String, Double> getJarakKeTetangga() {
        return jarakKeTetangga;
    }

    public void setJarakKeTetangga(HashMap<String, Double> jarakKeTetangga) {
        this.jarakKeTetangga = jarakKeTetangga;
    }
}
