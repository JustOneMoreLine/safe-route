package com.restuibu.saferoute.core;

import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Tabel berisi level tiap kecamatan
 */
@NoArgsConstructor
@Table(name = "datalevel")
@Entity
public class DataLevel {
    @Id
    @Column
    String kecamatan;
    @Column
    int levelSekarang; // level bulan terakhir
    @Column
    int levelSebelumnya; // level bulan sebelumnya
    @Column
    long bulan; // bulan data didapat
    @Column
    long tahun; // tahun data didapat

    public DataLevel(String kecamatan,
                     int levelSekarang,
                     int levelSebelumnya,
                     long bulan,
                     long tahun){
        this.kecamatan = kecamatan;
        this.levelSekarang = levelSekarang;
        this.levelSebelumnya = levelSebelumnya;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public int getLevelSekarang() {
        return levelSekarang;
    }

    public void setLevelSekarang(int levelSekarang) {
        this.levelSekarang = levelSekarang;
    }

    public int getLevelSebelumnya() {
        return levelSebelumnya;
    }

    public void setLevelSebelumnya(int levelSebelumnya) {
        this.levelSebelumnya = levelSebelumnya;
    }

}
