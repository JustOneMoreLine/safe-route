package com.restuibu.saferoute.core;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Tabel berisi data kecamatan dan populasinya
 */
@NoArgsConstructor
@Table(name = "datawilayah")
@Entity
public class DataWilayah {
    @Id
    @Column
    String kecamatan;
    @Column
    long populasi;

    public DataWilayah(String kecamatan,
                       long populasi){
        this.populasi = populasi;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public long getPopulasi() {
        return populasi;
    }

    public void setPopulasi(long populasi) {
        this.populasi = populasi;
    }
}
