package com.restuibu.saferoute.core;

import com.restuibu.saferoute.service.DatabaseServiceImpl;

import java.util.ArrayList;

/**
 * Kelas untuk A* Algo Safe Route
 */
public class Path {
    private String awal;
    private String tujuan;
    private ArrayList<String> path;

    public Path(String awal, String tujuan) {
        this.awal = awal;
        this.tujuan = tujuan;
        this.path = new ArrayList<>();
        this.path.add(awal);
    }

    public Path createNewPathWithThisCity(String newCity) {
        Path newPath = new Path(this.awal, this.tujuan);
        newPath.path = new ArrayList<>(this.path);
        newPath.path.add(newCity);
        return newPath;
    }

    public Double getJarak() {
        double result = 0;
        for(int i=0; i < this.path.size() - 1; i++) {
            String current = this.path.get(i);
            String next = this.path.get(i+1);
            //System.out.println("result: " + result);
            result += DatabaseServiceImpl.getJarak(current, next);
        }
        return result;
    }

    // Pure jarak
    public double getNilaiHeuristik() {
        double result = this.getJarak();
        String lastNode = this.path.get(this.path.size()-1);
        return result+DatabaseServiceImpl.nilaiHeuristik(lastNode,this.tujuan);
    }

    // With Corona level
    public double getNilaiHeuristik2() {
        double result = this.getJarak();
        // TODO : Tambah heuristik level corona ke jarak asli yaaa
        // f(x) = (g(x) * levelCorona) + h(x)
        // g(x) = jarak asli ke tetannga berikutnya
        // h(x) = heuristik jarak ke tujuan
        // levelCorona = levelCorona ke tetangga berikutnya
        String lastNode = this.path.get(this.path.size()-1);
        if(path.size() > 1) {
            int level = DatabaseServiceImpl.nilaiLevel(lastNode);
            String nextToLastNode = this.path.get(this.path.size()-2);
            double jarak = DatabaseServiceImpl.getJarak(nextToLastNode, lastNode);
            result -= jarak;
            result += jarak * level;
        }
        return result + DatabaseServiceImpl.nilaiHeuristik(lastNode,this.tujuan);
    }

    public String getLast() {
        return this.path.get(this.path.size()-1);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Path)) return false;
        String thisLastNode = this.getLast();
        String objLastNode = ((Path) obj).getLast();
        return thisLastNode.equals(objLastNode);
    }

    public String toString() {
        String result=this.awal;
        for(String kota : this.path) {
            if(kota.equals(this.awal)) continue;
            result = result + "-" + kota;
        }
        return result;
    }

    public ArrayList<String> getPath() {
        return this.path;
    }
}
