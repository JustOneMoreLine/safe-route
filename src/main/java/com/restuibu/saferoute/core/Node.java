/*package com.restuibu.saferoute.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import net.bytebuddy.implementation.bind.annotation.Default;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
@Entity
public class Node{
    @Id
    private String nama;
    @NotBlank(message = "population can't be null")
    private int population;
    @NotBlank(message = "newcases can't be null")
    private int newcases;
    @NotBlank(message = "last week's growth rate can't be null")
    private int growth_rate_lastweek;
    @NotBlank(message ="this week's growth rate can't be null")
    private int growth_rate_current;
    private int trajectory;
    @NotBlank(message = "risk rate can't be null")
    private int riskrate;
    @NotBlank(message = "number of infected can't be null")
    private int total_case;
    private int incidence;
    private int incidencerate;
    private ArrayList<Node> listofcities = new ArrayList<Node>();
    //temp fx
    private int fx = 0;
    //LIST SEMUA KOTA LAIN
    public Node(String nama, int population){
        this.nama = nama;
        this.population = population;
    }
    public void update (int newgrowth_rate){
        this.growth_rate_lastweek = this.growth_rate_current;
        this.growth_rate_current = newgrowth_rate;
        this.total_case += this.growth_rate_current;
        this.incidence = this.population/100000;
        find_trajectory();
        find_riskrate();
        find_incidence();
    }
    public void find_trajectory(){
        if(this.growth_rate_current > this.growth_rate_lastweek){
            this.trajectory = 1;
        }
        else if(this.growth_rate_current == this.growth_rate_lastweek){
            this.trajectory = 0;
        }
        else if(this.growth_rate_current < this.growth_rate_lastweek){
            this.trajectory = -1;
        }
    }
    public void find_riskrate(){
        if(this.growth_rate_current > 500){
            this.riskrate = 3;
        }
        else if(this.growth_rate_current > 250){
            this.riskrate = 2;
        }
        else if(this.growth_rate_current >= 50){
            this.riskrate = 1;
        }
        else{
            this.riskrate = 0;
        }

    }
    public void find_incidence(){
        if(this.incidence > 3){
            this.incidencerate = 3;
        }
        else if(this.incidence > 1.5){
            this.incidencerate = 2;
        }
        else{
            this.incidencerate = 1;
        }

    }
    public void addNeighbour(Node x){
        this.listofcities.add(x);
    }

    public int getPopulation() {
        return this.population;
    }

    public int getNewcases() {
        return this.newcases;
    }

    public int getTrajectory() {
        return this.trajectory;
    }
    public ArrayList getListOfCities(){
        return this.listofcities;
    }
    public void setFx(int x){
        this.fx = x;
    }
    public int getFx(){
        return this.fx;
    }
}*/