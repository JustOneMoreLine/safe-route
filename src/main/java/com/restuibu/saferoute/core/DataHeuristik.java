package com.restuibu.saferoute.core;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Database nyimpan jarak heuristik sebuah kecamatan ke seluruh
 * kecamatan lainnya.
 */
@NoArgsConstructor
@Table(name = "dataheuristik")
@Entity
public class DataHeuristik implements Serializable {
    @Id
    @Column(name = "id")
    String kecamatan;

    @ElementCollection
    @CollectionTable(name = "kecamatan_jarak_map_heu",
            joinColumns = {@JoinColumn(name = "data_heuristik_map", referencedColumnName = "id")})
    @MapKeyColumn(name = "kecamatan_name")
    @Column(name = "jarak")
    Map<String, Double> jarakKeTetangga;

    public DataHeuristik(String kecamatan, Map<String, Double> jarakKeTetangga) {
        this.kecamatan = kecamatan;
        this.jarakKeTetangga = jarakKeTetangga;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Map<String, Double> getJarakKeTetangga() {
        return jarakKeTetangga;
    }

    public void setJarakKeTetangga(HashMap<String, Double> jarakKeTetangga) {
        this.jarakKeTetangga = jarakKeTetangga;
    }


}
